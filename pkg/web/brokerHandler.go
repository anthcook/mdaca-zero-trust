package web

import (
	"fmt"
	"log"
	"strings"

	"mdaca-zero-trust/pkg/aws"
	"mdaca-zero-trust/pkg/crypter"
	"mdaca-zero-trust/pkg/parameters"
	"mdaca-zero-trust/pkg/vault"
)

// Web service handler implementation
type BrokerServiceHandler struct{}

func (s BrokerServiceHandler) SecureKeys(keys *VaultKeys) (bool, ServiceError) {
	path := vault.StoragePath(keys.NodeID)
	vaultBox := vault.NewBox(path, keys.NodeID)
	vaultBox.Secrets[vault.EncryptionKeyName] = keys.SymmetricKey
	vaultBox.Secrets[vault.SignatureKeyName] = keys.PublicKey
	success, err := vaultClient.StoreBox(path, vaultBox)
	if err != nil {
		logBrokerHandler(fmt.Sprintf("%v", err))
		return success, KeyStorageServerError
	}

	return success, (ServiceError{})
}

func (s BrokerServiceHandler) DeleteKeys(keys *VaultKeys) (bool, ServiceError) {
	path := vault.StoragePath(keys.NodeID)
	success, err := vaultClient.EmptyBox(path)
	if err != nil {
		logBrokerHandler(fmt.Sprintf("%v", err))
		return success, KeyStorageServerError
	}

	return success, (ServiceError{})
}

func (s BrokerServiceHandler) InventoryKeys(keys *VaultKeys) ([]string, ServiceError) {
	path := vault.StoragePath(keys.NodeID)
	success, err := vaultClient.InventoryBox(path)
	if err != nil {
		logBrokerHandler(fmt.Sprintf("%v", err))
		return success, KeyStorageServerError
	}

	return success, (ServiceError{})
}

func (s BrokerServiceHandler) OpenKeys(keys *VaultKeys) (*vault.VaultBox, ServiceError) {
	path := vault.StoragePath(keys.NodeID)
	success, err := vaultClient.OpenBox(path)
	if err != nil {
		logBrokerHandler(fmt.Sprintf("%v", err))
		return success, KeyStorageServerError
	}

	return success, (ServiceError{})
}

func (s BrokerServiceHandler) UpdateCustomer(cust *Customer) (bool, ServiceError) {
	success, err := dbClient.SetDeviceOwner(cust.NodeID, cust.CustomerID, cust.AccountID)
	if err != nil {
		logBrokerHandler(fmt.Sprintf("%v", err))
		return success, CustomerUpdateServerError
	}

	return true, (ServiceError{})
}

func (s BrokerServiceHandler) ValidateData(blocks *NodeBlocks) ([]ValidationResult, ServiceError) {
	var results []ValidationResult

	cnt := len(blocks.Data)
	if cnt < 1 {
		return results, ValidationNoData
	}

	results = make([]ValidationResult, cnt)
	for i, data := range blocks.Data {
		validated, nodeBlock, err := crypter.ValidateEncodedData(data, vaultClient)
		if err != (crypter.CrypterError{}) {
			logBrokerHandler(err.Message)

			switch err.Code {
			case crypter.ParsingError:
				return results, ValidationBadData
			case crypter.VaultError:
				logBrokerHandler("TODO: broker.ValidateData - add error block to service results: VaultError")
			case crypter.ValidationError:
				logBrokerHandler("TODO: broker.ValidateData - add error block to service results: ValidationError")
			}
		}

		results[i] = ValidationResult{
			NodeID:    nodeBlock.NodeIDValue(),
			Timestamp: nodeBlock.TimestampValue(),
			Digest:    nodeBlock.BlockDigestValue(),
			Validated: validated,
		}
	}

	return results, (ServiceError{})
}

func (s BrokerServiceHandler) DecryptData(blocks *NodeBlocks) ([]DecryptionResult, ServiceError) {
	var results []DecryptionResult

	cnt := len(blocks.Data)
	if cnt < 1 {
		return results, DecryptionNoData
	}

	results = make([]DecryptionResult, cnt)
	for i, data := range blocks.Data {
		encodedValue, nodeBlock, err := crypter.DecryptEncodedData(data, vaultClient)
		if err != (crypter.CrypterError{}) {
			logBrokerHandler(err.Message)

			switch err.Code {
			case crypter.ParsingError:
				return results, DecryptionBadData
			case crypter.VaultError:
				logBrokerHandler("TODO: broker.DecryptData - add error block to service results")
			case crypter.ValidationError:
				logBrokerHandler("TODO: broker.DecryptData - add error block to service results")
			}
		}

		results[i] = DecryptionResult{
			NodeID:    nodeBlock.NodeIDValue(),
			Timestamp: nodeBlock.TimestampValue(),
			Digest:    nodeBlock.BlockDigestValue(),
			Data:      encodedValue,
		}
	}

	return results, (ServiceError{})
}

func InitBrokerHandler() bool {
	if loaded, err := parameters.Parse(); !loaded {
		if err != nil {
			log.Fatal(fmt.Sprintf("Error parsing runtime parameters: %v", err))
		} else {
			logBrokerHandler("(WARN) Failed to parse runtime parameters.")
		}
	}

	if !initAWSSession() {
		return false
	}

	initBillingDBClient()
	initVaultClient()

	return true
}

var vaultClient vault.VaultClient
var dbClient aws.BillingDBClient

func initVaultClient() {
	var vtype string
	if strings.ToLower(parameters.Parameter(parameters.BROKER_TEST_MODE)) == "true" {
		vaultClient = vault.Client(vault.MockVaultClient)
		vtype = "mock"
	} else {
		vaultClient = vault.Client(vault.RealVaultClient)
		vtype = "real"
	}

	logBrokerHandler(fmt.Sprintf("Vault client initialized (type = %v).", vtype))
}

func initAWSSession() bool {
	sessionConfig := aws.LoadParameters()

	success, err := aws.InitClientSession(sessionConfig)

	if err != nil {
		logBrokerHandler(fmt.Sprintf("Error initializing AWS session: %v", err))
	} else {
		logBrokerHandler("AWS client session initialized.")
	}

	return success
}

func initBillingDBClient() {
	var dbUrl string
	if parameters.AssignedValue(parameters.BROKER_DB_ADDRESS) {
		dbUrl = parameters.Parameter(parameters.BROKER_DB_ADDRESS)
	}
	dbClient = aws.NewBillingDBClient(dbUrl)

	logBrokerHandler("Billing database client initialized.")
}

func logBrokerHandler(message string) {
	log.Println(fmt.Sprintf("BrokerHandler - %v", message))
}

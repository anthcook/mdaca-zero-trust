package parameters

import (
	"errors"
	"flag"
	"os"

	"github.com/magiconair/properties"
)

const (
	CONFIG_FILE ParameterEnum = iota
	AWS_PROFILE
	AWS_ACCESS_KEY
	AWS_SECRET_KEY
	AWS_SESSION_TOKEN
	AWS_REGION
	BROKER_TEST_MODE
	BROKER_VAULT_ADDRESS
	BROKER_VAULT_KEY
	BROKER_VAULT_TOKEN
	BROKER_DB_ADDRESS
	CLIENT_SERVICE_URL
	CLIENT_QUEUE_URL
	CLIENT_QUEUE_TYPE
	CLIENT_QUEUE_NAME
	CLIENT_SEND_INTERVAL
	CLIENT_OUTPUT_PATH
	ROUTER_S3_BUCKETNAME
	ROUTER_KAFKA_BROKER_1
	ROUTER_KAFKA_BROKER_2
	ROUTER_KAFKA_BROKER_3
	ROUTER_KAFKA_QUEUE_TYPE
	ROUTER_KAFKA_QUEUE_TIME
	ROUTER_KAFKA_QUEUE_LENGTH
	ROUTER_MQTT_ADDRESS
	ROUTER_KAFKA_ID
	ROUTER_KAFKA_TOPIC
	ROUTER_MINIO_ENDPOINT
	ROUTER_MINIO_ID
	ROUTER_MINIO_ACCESSKEY
)

const (
	ENV_CONFIG_FILE               = "VZT_CONFIG_FILE"
	ENV_AWS_PROFILE               = "VZT_AWS_PROFILE"
	ENV_AWS_ACCESS_KEY            = "VZT_AWS_ACCESS_KEY"
	ENV_AWS_SECRET_KEY            = "VZT_AWS_SECRET_KEY"
	ENV_AWS_SESSION_TOKEN         = "VZT_AWS_SESSION_TOKEN"
	ENV_AWS_REGION                = "VZT_AWS_REGION"
	ENV_BROKER_TEST_MODE          = "VZT_BROKER_TEST_MODE"
	ENV_BROKER_VAULT_ADDRESS      = "VZT_BROKER_VAULT_ADDRESS"
	ENV_BROKER_VAULT_KEY          = "VZT_BROKER_VAULT_KEY"
	ENV_BROKER_VAULT_TOKEN        = "VZT_BROKER_VAULT_TOKEN"
	ENV_BROKER_DB_ADDRESS         = "VZT_BROKER_DB_ADDRESS"
	ENV_CLIENT_SERVICE_URL        = "VZT_CLIENT_SERVICE_URL"
	ENV_CLIENT_QUEUE_URL          = "VZT_CLIENT_QUEUE_URL"
	ENV_CLIENT_QUEUE_TYPE         = "VZT_CLIENT_QUEUE_TYPE"
	ENV_CLIENT_QUEUE_NAME         = "VZT_CLIENT_QUEUE_NAME"
	ENV_CLIENT_SEND_INTERVAL      = "VZT_CLIENT_SEND_INTERVAL"
	ENV_CLIENT_OUTPUT_PATH        = "VZT_CLIENT_OUTPUT_PATH"
	ENV_ROUTER_S3_BUCKETNAME      = "VZT_ROUTER_S3_BUCKETNAME"
	ENV_ROUTER_KAFKA_BROKER_1     = "VZT_ROUTER_KAFKA_BROKER_1"
	ENV_ROUTER_KAFKA_BROKER_2     = "VZT_ROUTER_KAFKA_BROKER_2"
	ENV_ROUTER_KAFKA_BROKER_3     = "VZT_ROUTER_KAFKA_BROKER_3"
	ENV_ROUTER_KAFKA_QUEUE_TYPE   = "VZT_ROUTER_KAFKA_QUEUE_TYPE"
	ENV_ROUTER_KAFKA_QUEUE_TIME   = "VZT_ROUTER_KAFKA_QUEUE_TIME"
	ENV_ROUTER_KAFKA_QUEUE_LENGTH = "VZT_ROUTER_KAFKA_QUEUE_LENGTH"
	ENV_ROUTER_MQTT_ADDRESS       = "VZT_ROUTER_MQTT_ADDRESS"
	ENV_ROUTER_KAFKA_ID           = "VZT_ROUTER_KAFKA_ID"
	ENV_ROUTER_KAFKA_TOPIC        = "VZT_ROUTER_KAFKA_TOPIC"
	ENV_ROUTER_MINIO_ENDPOINT     = "VZT_ROUTER_MINIO_ENDPOINT"
	ENV_ROUTER_MINIO_ID           = "VZT_ROUTER_MINIO_ID"
	ENV_ROUTER_MINIO_ACCESSKEY    = "VZT_ROUTER_MINIO_ACCESSKEY"
)

const USER_MUST_PROVIDE = "userMustProvide"

type ParameterEnum int

func Parameter(parm ParameterEnum) string {
	switch parm {
	case CONFIG_FILE:
		return config.config_file
	case AWS_PROFILE:
		return config.aws_profile
	case AWS_ACCESS_KEY:
		return config.aws_access_key
	case AWS_SECRET_KEY:
		return config.aws_secret_key
	case AWS_SESSION_TOKEN:
		return config.aws_session_token
	case AWS_REGION:
		return config.aws_region
	case BROKER_TEST_MODE:
		return config.broker_test_mode
	case BROKER_VAULT_ADDRESS:
		return config.broker_vault_address
	case BROKER_VAULT_KEY:
		return config.broker_vault_key
	case BROKER_VAULT_TOKEN:
		return config.broker_vault_token
	case BROKER_DB_ADDRESS:
		return config.broker_dynamo_address
	case CLIENT_SERVICE_URL:
		return config.client_broker_server
	case CLIENT_QUEUE_URL:
		return config.client_queue_server
	case CLIENT_QUEUE_TYPE:
		return config.client_queue_type
	case CLIENT_QUEUE_NAME:
		return config.client_queue_name
	case CLIENT_SEND_INTERVAL:
		return config.client_message_interval
	case CLIENT_OUTPUT_PATH:
		return config.output
	case ROUTER_S3_BUCKETNAME:
		return config.router_s3_bucketname
	case ROUTER_KAFKA_BROKER_1:
		return config.router_kafka_broker_1
	case ROUTER_KAFKA_BROKER_2:
		return config.router_kafka_broker_2
	case ROUTER_KAFKA_BROKER_3:
		return config.router_kafka_broker_3
	case ROUTER_KAFKA_QUEUE_TYPE:
		return config.router_kafka_queue_type
	case ROUTER_KAFKA_QUEUE_TIME:
		return config.router_kafka_queue_time
	case ROUTER_KAFKA_QUEUE_LENGTH:
		return config.router_kafka_queue_length
	case ROUTER_MQTT_ADDRESS:
		return config.router_mqtt_address
	case ROUTER_KAFKA_ID:
		return config.router_kafka_id
	case ROUTER_KAFKA_TOPIC:
		return config.router_kafka_topic
	case ROUTER_MINIO_ENDPOINT:
		return config.router_minio_endpoint
	case ROUTER_MINIO_ID:
		return config.router_minio_id
	case ROUTER_MINIO_ACCESSKEY:
		return config.router_minio_accesskey
	}

	return ""
}

func ParameterWithDefault(parm ParameterEnum, defVal string) string {
	parmVal := Parameter(parm)
	if parmVal == USER_MUST_PROVIDE {
		parmVal = defVal
	}

	return parmVal
}

func AssignedValue(field ParameterEnum) bool {
	rval := true
	if Parameter(field) == USER_MUST_PROVIDE {
		rval = false
	}
	return rval
}

// Configuration precedence:
// 1. CLI arguments
// 2. Environment variables
// 3. Configuration properties
//
func Parse() (bool, error) {
	// Loading environment variables before parsing CLI arguments lets
	// environment values serve as defaults for the CLI parser, while
	// still letting the CLI arguments trump variables...
	//
	loadEnvironment()
	parseArguments()

	if AssignedValue(CONFIG_FILE) {
		// ...after which, configuration properties can define any parameters
		// not already assigned.
		//
		success, err := loadProperties(config.config_file)
		if err != nil {
			return success, err
		}
	}

	return true, nil
}

var parameters = [...]parameterValues{
	{"config", "config", ENV_CONFIG_FILE, "path to configuration properties file"},
	{"aws.profile", "profile", ENV_AWS_PROFILE, "AWS IAM credentials profile"},
	{"aws.accessKey", "accessKey", ENV_AWS_ACCESS_KEY, "AWS IAM account access key"},
	{"aws.secretKey", "secretKey", ENV_AWS_SECRET_KEY, "AWS IAM account secret key"},
	{"aws.sessionToken", "sessionToken", ENV_AWS_SESSION_TOKEN, "AWS temporary access token"},
	{"aws.region", "region", ENV_AWS_REGION, "AWS account region"},
	{"broker.testMode", "testMode", ENV_BROKER_TEST_MODE, "whether broker runs in production mode"},
	{"broker.vaultAddress", "vaultAddress", ENV_BROKER_VAULT_ADDRESS, "URL of Hashicorp vault instance"},
	{"broker.vaultKey", "vaultKey", ENV_BROKER_VAULT_KEY, "Hashicorp vault instance key"},
	{"broker.vaultToken", "vaultToken", ENV_BROKER_VAULT_TOKEN, "Hashicorp vault instance user token"},
	{"broker.dynamoAddress", "ddbAddress", ENV_BROKER_DB_ADDRESS, "URL of local DynamoDB instance"},
	{"client.broker.server", "brokerServer", ENV_CLIENT_SERVICE_URL, "URL of the broker server"},
	{"client.queue.server", "queueServer", ENV_CLIENT_QUEUE_URL, "URL of the SQS or MQTT messaging server"},
	{"client.queue.type", "queueType", ENV_CLIENT_QUEUE_TYPE, "the type of messaging server: SQS, MQTT"},
	{"client.queue.name", "queueName", ENV_CLIENT_QUEUE_NAME, "the messaging server queue or topic name"},
	{"client.message.interval", "messageInterval", ENV_CLIENT_SEND_INTERVAL, "the period between automated node block generation"},
	{"output", "output", ENV_CLIENT_OUTPUT_PATH, "file name for client response"},
	{"router.bucketName", "bucketName", ENV_ROUTER_S3_BUCKETNAME, "S3 Bucket to be used by the router"},
	{"router.kafka.broker1", "broker1", ENV_ROUTER_KAFKA_BROKER_1, "Address to one of the three kafka clusters to be used"},
	{"router.kafka.broker2", "broker2", ENV_ROUTER_KAFKA_BROKER_2, "Address to one of the three kafka clusters to be used"},
	{"router.kafka.broker3", "broker3", ENV_ROUTER_KAFKA_BROKER_3, "Address to one of the three kafka clusters to be used"},
	{"router.kafka.queue.type", "kafkaType", ENV_ROUTER_KAFKA_QUEUE_TYPE, "Polling method to be used for the Kafka messages(Instant, Batched, Timed)"},
	{"router.kafka.queue.time", "kafkaTime", ENV_ROUTER_KAFKA_QUEUE_TIME, "Only used for timed kafka queues, interval to wait between polls"},
	{"router.kafka.queue.length", "kafkaLength", ENV_ROUTER_KAFKA_QUEUE_LENGTH, "Only used for timed kafka queues, lenght of the queue needed to send messages"},
	{"router.MQTT.address", "mqttAddress", ENV_ROUTER_MQTT_ADDRESS, "file name for client response"},
	{"router.kafka.id", "kafkaID", ENV_ROUTER_KAFKA_ID, "ClientID to be used by the kafka client to send topics"},
	{"router.kafka.topic", "topic", ENV_ROUTER_KAFKA_TOPIC, "Topic name to subscribe to."},
	{"router.minio.endpoint", "endpoint", ENV_ROUTER_MINIO_ENDPOINT, "MINiO connection endpoint"},
	{"router.minio.id", "minioID", ENV_ROUTER_MINIO_ID, "MINiO ID to be used for credentials"},
	{"router.minio.accessKey", "minioKey", ENV_ROUTER_MINIO_ACCESSKEY, "MINiO password to be used for credentials"},
}

type parameterValues struct {
	property string
	argument string
	variable string
	usage    string
}

type configuration struct {
	config_file               string
	aws_profile               string
	aws_access_key            string
	aws_secret_key            string
	aws_session_token         string
	aws_region                string
	broker_test_mode          string
	broker_vault_address      string
	broker_vault_key          string
	broker_vault_token        string
	broker_dynamo_address     string
	client_broker_server      string
	client_queue_server       string
	client_queue_type         string
	client_queue_name         string
	client_message_interval   string
	output                    string
	router_s3_bucketname      string
	router_kafka_broker_1     string
	router_kafka_broker_2     string
	router_kafka_broker_3     string
	router_kafka_queue_type   string
	router_kafka_queue_time   string
	router_kafka_queue_length string
	router_mqtt_address       string
	router_kafka_id           string
	router_kafka_topic        string
	router_minio_endpoint     string
	router_minio_id           string
	router_minio_accesskey    string
}

var config = configuration{
	config_file:               USER_MUST_PROVIDE,
	aws_profile:               USER_MUST_PROVIDE,
	aws_access_key:            USER_MUST_PROVIDE,
	aws_secret_key:            USER_MUST_PROVIDE,
	aws_session_token:         USER_MUST_PROVIDE,
	aws_region:                USER_MUST_PROVIDE,
	broker_test_mode:          USER_MUST_PROVIDE,
	broker_vault_address:      USER_MUST_PROVIDE,
	broker_vault_key:          USER_MUST_PROVIDE,
	broker_vault_token:        USER_MUST_PROVIDE,
	broker_dynamo_address:     USER_MUST_PROVIDE,
	client_broker_server:      USER_MUST_PROVIDE,
	client_queue_server:       USER_MUST_PROVIDE,
	client_queue_type:         USER_MUST_PROVIDE,
	client_queue_name:         USER_MUST_PROVIDE,
	client_message_interval:   USER_MUST_PROVIDE,
	output:                    USER_MUST_PROVIDE,
	router_s3_bucketname:      USER_MUST_PROVIDE,
	router_kafka_broker_1:     USER_MUST_PROVIDE,
	router_kafka_broker_2:     USER_MUST_PROVIDE,
	router_kafka_broker_3:     USER_MUST_PROVIDE,
	router_kafka_queue_type:   USER_MUST_PROVIDE,
	router_kafka_queue_time:   USER_MUST_PROVIDE,
	router_kafka_queue_length: USER_MUST_PROVIDE,
	router_mqtt_address:       USER_MUST_PROVIDE,
	router_kafka_id:           USER_MUST_PROVIDE,
	router_kafka_topic:        USER_MUST_PROVIDE,
	router_minio_endpoint:     USER_MUST_PROVIDE,
	router_minio_id:           USER_MUST_PROVIDE,
	router_minio_accesskey:    USER_MUST_PROVIDE,
}

func loadProperties(filename string) (bool, error) {
	if filename == "" {
		return false, errors.New("File name not provided.")
	}

	props, err := properties.LoadFile(filename, properties.UTF8)
	if err != nil {
		return false, err
	}

	config.aws_profile = configurationValue(props, parameters[AWS_PROFILE].property, config.aws_profile, USER_MUST_PROVIDE)
	config.aws_access_key = configurationValue(props, parameters[AWS_ACCESS_KEY].property, config.aws_access_key, USER_MUST_PROVIDE)
	config.aws_secret_key = configurationValue(props, parameters[AWS_SECRET_KEY].property, config.aws_secret_key, USER_MUST_PROVIDE)
	config.aws_session_token = configurationValue(props, parameters[AWS_SESSION_TOKEN].property, config.aws_session_token, USER_MUST_PROVIDE)
	config.aws_region = configurationValue(props, parameters[AWS_REGION].property, config.aws_region, USER_MUST_PROVIDE)
	config.broker_test_mode = configurationValue(props, parameters[BROKER_TEST_MODE].property, config.broker_test_mode, USER_MUST_PROVIDE)
	config.broker_vault_address = configurationValue(props, parameters[BROKER_VAULT_ADDRESS].property, config.broker_vault_address, USER_MUST_PROVIDE)
	config.broker_vault_key = configurationValue(props, parameters[BROKER_VAULT_KEY].property, config.broker_vault_key, USER_MUST_PROVIDE)
	config.broker_vault_token = configurationValue(props, parameters[BROKER_VAULT_TOKEN].property, config.broker_vault_token, USER_MUST_PROVIDE)
	config.broker_dynamo_address = configurationValue(props, parameters[BROKER_DB_ADDRESS].property, config.broker_dynamo_address, USER_MUST_PROVIDE)
	config.client_broker_server = configurationValue(props, parameters[CLIENT_SERVICE_URL].property, config.client_broker_server, USER_MUST_PROVIDE)
	config.client_queue_server = configurationValue(props, parameters[CLIENT_QUEUE_URL].property, config.client_queue_server, USER_MUST_PROVIDE)
	config.client_queue_type = configurationValue(props, parameters[CLIENT_QUEUE_TYPE].property, config.client_queue_type, USER_MUST_PROVIDE)
	config.client_queue_name = configurationValue(props, parameters[CLIENT_QUEUE_NAME].property, config.client_queue_name, USER_MUST_PROVIDE)
	config.client_message_interval = configurationValue(props, parameters[CLIENT_SEND_INTERVAL].property, config.client_message_interval, USER_MUST_PROVIDE)
	config.output = configurationValue(props, parameters[CLIENT_OUTPUT_PATH].property, config.output, USER_MUST_PROVIDE)
	config.router_s3_bucketname = configurationValue(props, parameters[ROUTER_S3_BUCKETNAME].property, config.router_s3_bucketname, USER_MUST_PROVIDE)
	config.router_kafka_broker_1 = configurationValue(props, parameters[ROUTER_KAFKA_BROKER_1].property, config.router_kafka_broker_1, USER_MUST_PROVIDE)
	config.router_kafka_broker_2 = configurationValue(props, parameters[ROUTER_KAFKA_BROKER_2].property, config.router_kafka_broker_2, USER_MUST_PROVIDE)
	config.router_kafka_broker_3 = configurationValue(props, parameters[ROUTER_KAFKA_BROKER_3].property, config.router_kafka_broker_3, USER_MUST_PROVIDE)
	config.router_kafka_queue_type = configurationValue(props, parameters[ROUTER_KAFKA_QUEUE_TYPE].property, config.router_kafka_queue_type, USER_MUST_PROVIDE)
	config.router_kafka_queue_time = configurationValue(props, parameters[ROUTER_KAFKA_QUEUE_TIME].property, config.router_kafka_queue_time, USER_MUST_PROVIDE)
	config.router_kafka_queue_length = configurationValue(props, parameters[ROUTER_KAFKA_QUEUE_LENGTH].property, config.router_kafka_queue_length, USER_MUST_PROVIDE)
	config.router_mqtt_address = configurationValue(props, parameters[ROUTER_MQTT_ADDRESS].property, config.router_mqtt_address, USER_MUST_PROVIDE)
	config.router_kafka_id = configurationValue(props, parameters[ROUTER_KAFKA_ID].property, config.router_kafka_id, USER_MUST_PROVIDE)
	config.router_kafka_topic = configurationValue(props, parameters[ROUTER_KAFKA_TOPIC].property, config.router_kafka_topic, USER_MUST_PROVIDE)
	config.router_minio_endpoint = configurationValue(props, parameters[ROUTER_MINIO_ENDPOINT].property, config.router_minio_endpoint, USER_MUST_PROVIDE)
	config.router_minio_id = configurationValue(props, parameters[ROUTER_MINIO_ENDPOINT].property, config.router_minio_id, USER_MUST_PROVIDE)
	config.router_minio_accesskey = configurationValue(props, parameters[ROUTER_MINIO_ID].property, config.router_minio_accesskey, USER_MUST_PROVIDE)

	// Need to hard set these in the event the user didn't provide at any
	// config level...
	config.broker_test_mode = ParameterWithDefault(BROKER_TEST_MODE, "false")
	config.client_message_interval = ParameterWithDefault(CLIENT_SEND_INTERVAL, "60")
	config.router_kafka_queue_time = ParameterWithDefault(ROUTER_KAFKA_QUEUE_TIME, "20")
	config.router_kafka_queue_length = ParameterWithDefault(ROUTER_KAFKA_QUEUE_LENGTH, "10")

	return true, nil
}

func configurationValue(props *properties.Properties, key string, argVal string, defVal string) string {
	if argVal == USER_MUST_PROVIDE || argVal == defVal {
		propVal := props.GetString(key, defVal)
		if propVal == USER_MUST_PROVIDE {
			return defVal
		}
		return propVal
	}
	return argVal
}

func parseArguments() {
	flag.StringVar(&config.config_file, parameters[CONFIG_FILE].argument, config.config_file, parameters[CONFIG_FILE].usage)
	flag.StringVar(&config.aws_access_key, parameters[AWS_ACCESS_KEY].argument, config.aws_access_key, parameters[AWS_ACCESS_KEY].usage)
	flag.StringVar(&config.aws_secret_key, parameters[AWS_SECRET_KEY].argument, config.aws_secret_key, parameters[AWS_SECRET_KEY].usage)
	flag.StringVar(&config.aws_session_token, parameters[AWS_SESSION_TOKEN].argument, config.aws_session_token, parameters[AWS_SESSION_TOKEN].usage)
	flag.StringVar(&config.aws_region, parameters[AWS_REGION].argument, config.aws_region, parameters[AWS_REGION].usage)
	flag.StringVar(&config.broker_test_mode, parameters[BROKER_TEST_MODE].argument, config.broker_test_mode, parameters[BROKER_TEST_MODE].usage)
	flag.StringVar(&config.broker_vault_address, parameters[BROKER_VAULT_ADDRESS].argument, config.broker_vault_address, parameters[BROKER_VAULT_ADDRESS].usage)
	flag.StringVar(&config.broker_vault_key, parameters[BROKER_VAULT_KEY].argument, config.broker_vault_key, parameters[BROKER_VAULT_KEY].usage)
	flag.StringVar(&config.broker_vault_token, parameters[BROKER_VAULT_TOKEN].argument, config.broker_vault_token, parameters[BROKER_VAULT_TOKEN].usage)
	flag.StringVar(&config.broker_dynamo_address, parameters[BROKER_DB_ADDRESS].argument, config.broker_dynamo_address, parameters[BROKER_DB_ADDRESS].usage)
	flag.StringVar(&config.client_broker_server, parameters[CLIENT_SERVICE_URL].argument, config.client_broker_server, parameters[CLIENT_SERVICE_URL].usage)
	flag.StringVar(&config.client_queue_server, parameters[CLIENT_QUEUE_URL].argument, config.client_queue_server, parameters[CLIENT_QUEUE_URL].usage)
	flag.StringVar(&config.client_queue_type, parameters[CLIENT_QUEUE_TYPE].argument, config.client_queue_type, parameters[CLIENT_QUEUE_TYPE].usage)
	flag.StringVar(&config.client_queue_name, parameters[CLIENT_QUEUE_NAME].argument, config.client_queue_name, parameters[CLIENT_QUEUE_NAME].usage)
	flag.StringVar(&config.client_message_interval, parameters[CLIENT_SEND_INTERVAL].argument, config.client_message_interval, parameters[CLIENT_SEND_INTERVAL].usage)
	flag.StringVar(&config.output, parameters[CLIENT_OUTPUT_PATH].argument, config.output, parameters[CLIENT_OUTPUT_PATH].usage)
	flag.StringVar(&config.router_s3_bucketname, parameters[ROUTER_S3_BUCKETNAME].argument, config.router_s3_bucketname, parameters[ROUTER_S3_BUCKETNAME].usage)
	flag.StringVar(&config.router_kafka_broker_1, parameters[ROUTER_KAFKA_BROKER_1].argument, config.router_kafka_broker_1, parameters[ROUTER_KAFKA_BROKER_1].usage)
	flag.StringVar(&config.router_kafka_broker_2, parameters[ROUTER_KAFKA_BROKER_2].argument, config.router_kafka_broker_2, parameters[ROUTER_KAFKA_BROKER_2].usage)
	flag.StringVar(&config.router_kafka_broker_3, parameters[ROUTER_KAFKA_BROKER_3].argument, config.router_kafka_broker_3, parameters[ROUTER_KAFKA_BROKER_3].usage)
	flag.StringVar(&config.router_kafka_queue_type, parameters[ROUTER_KAFKA_QUEUE_TYPE].argument, config.router_kafka_queue_type, parameters[ROUTER_KAFKA_QUEUE_TYPE].usage)
	flag.StringVar(&config.router_kafka_queue_time, parameters[ROUTER_KAFKA_QUEUE_TIME].argument, config.router_kafka_queue_time, parameters[ROUTER_KAFKA_QUEUE_TIME].usage)
	flag.StringVar(&config.router_kafka_queue_length, parameters[ROUTER_KAFKA_QUEUE_LENGTH].argument, config.router_kafka_queue_length, parameters[ROUTER_KAFKA_QUEUE_LENGTH].usage)
	flag.StringVar(&config.router_mqtt_address, parameters[ROUTER_MQTT_ADDRESS].argument, config.router_mqtt_address, parameters[ROUTER_MQTT_ADDRESS].usage)
	flag.StringVar(&config.router_kafka_id, parameters[ROUTER_KAFKA_ID].argument, config.router_kafka_id, parameters[ROUTER_KAFKA_ID].usage)
	flag.StringVar(&config.router_kafka_topic, parameters[ROUTER_KAFKA_TOPIC].argument, config.router_kafka_topic, parameters[ROUTER_KAFKA_TOPIC].usage)
	flag.StringVar(&config.router_minio_endpoint, parameters[ROUTER_MINIO_ENDPOINT].argument, config.router_minio_endpoint, parameters[ROUTER_MINIO_ENDPOINT].usage)
	flag.StringVar(&config.router_minio_id, parameters[ROUTER_MINIO_ID].argument, config.router_minio_id, parameters[ROUTER_MINIO_ID].usage)
	flag.StringVar(&config.router_minio_accesskey, parameters[ROUTER_MINIO_ACCESSKEY].argument, config.router_minio_accesskey, parameters[ROUTER_MINIO_ACCESSKEY].usage)
	flag.Parse()
}

func loadEnvironment() {
	if os.Getenv(ENV_CONFIG_FILE) != "" {
		config.config_file = os.Getenv(ENV_CONFIG_FILE)
	}
	if os.Getenv(ENV_AWS_PROFILE) != "" {
		config.aws_profile = os.Getenv(ENV_AWS_PROFILE)
	}
	if os.Getenv(ENV_AWS_ACCESS_KEY) != "" {
		config.aws_access_key = os.Getenv(ENV_AWS_ACCESS_KEY)
	}
	if os.Getenv(ENV_AWS_SECRET_KEY) != "" {
		config.aws_secret_key = os.Getenv(ENV_AWS_SECRET_KEY)
	}
	if os.Getenv(ENV_AWS_SESSION_TOKEN) != "" {
		config.aws_session_token = os.Getenv(ENV_AWS_SESSION_TOKEN)
	}
	if os.Getenv(ENV_AWS_REGION) != "" {
		config.aws_region = os.Getenv(ENV_AWS_REGION)
	}
	if os.Getenv(ENV_BROKER_TEST_MODE) != "" {
		config.broker_test_mode = os.Getenv(ENV_BROKER_TEST_MODE)
	}
	if os.Getenv(ENV_BROKER_VAULT_ADDRESS) != "" {
		config.broker_vault_address = os.Getenv(ENV_BROKER_VAULT_ADDRESS)
	}
	if os.Getenv(ENV_BROKER_VAULT_KEY) != "" {
		config.broker_vault_key = os.Getenv(ENV_BROKER_VAULT_KEY)
	}
	if os.Getenv(ENV_BROKER_VAULT_TOKEN) != "" {
		config.broker_vault_token = os.Getenv(ENV_BROKER_VAULT_TOKEN)
	}
	if os.Getenv(ENV_BROKER_DB_ADDRESS) != "" {
		config.broker_dynamo_address = os.Getenv(ENV_BROKER_DB_ADDRESS)
	}
	if os.Getenv(ENV_CLIENT_SERVICE_URL) != "" {
		config.client_broker_server = os.Getenv(ENV_CLIENT_SERVICE_URL)
	}
	if os.Getenv(ENV_CLIENT_QUEUE_URL) != "" {
		config.client_queue_server = os.Getenv(ENV_CLIENT_QUEUE_URL)
	}
	if os.Getenv(ENV_CLIENT_QUEUE_TYPE) != "" {
		config.client_queue_type = os.Getenv(ENV_CLIENT_QUEUE_TYPE)
	}
	if os.Getenv(ENV_CLIENT_QUEUE_NAME) != "" {
		config.client_queue_name = os.Getenv(ENV_CLIENT_QUEUE_NAME)
	}
	if os.Getenv(ENV_CLIENT_SEND_INTERVAL) != "" {
		config.client_message_interval = os.Getenv(ENV_CLIENT_SEND_INTERVAL)
	}
	if os.Getenv(ENV_CLIENT_OUTPUT_PATH) != "" {
		config.output = os.Getenv(ENV_CLIENT_OUTPUT_PATH)
	}
	if os.Getenv(ENV_ROUTER_S3_BUCKETNAME) != "" {
		config.output = os.Getenv(ENV_ROUTER_S3_BUCKETNAME)
	}
	if os.Getenv(ENV_ROUTER_KAFKA_BROKER_1) != "" {
		config.output = os.Getenv(ENV_ROUTER_KAFKA_BROKER_1)
	}
	if os.Getenv(ENV_ROUTER_KAFKA_BROKER_2) != "" {
		config.output = os.Getenv(ENV_ROUTER_KAFKA_BROKER_2)
	}
	if os.Getenv(ENV_ROUTER_KAFKA_BROKER_3) != "" {
		config.output = os.Getenv(ENV_ROUTER_KAFKA_BROKER_3)
	}
	if os.Getenv(ENV_ROUTER_KAFKA_QUEUE_TYPE) != "" {
		config.output = os.Getenv(ENV_ROUTER_KAFKA_QUEUE_TYPE)
	}
	if os.Getenv(ENV_ROUTER_KAFKA_QUEUE_TIME) != "" {
		config.output = os.Getenv(ENV_ROUTER_KAFKA_QUEUE_TIME)
	}
	if os.Getenv(ENV_ROUTER_KAFKA_QUEUE_LENGTH) != "" {
		config.output = os.Getenv(config.router_kafka_queue_length)
	}
	if os.Getenv(ENV_ROUTER_MQTT_ADDRESS) != "" {
		config.output = os.Getenv(ENV_ROUTER_MQTT_ADDRESS)
	}
	if os.Getenv(ENV_ROUTER_KAFKA_ID) != "" {
		config.output = os.Getenv(ENV_ROUTER_KAFKA_ID)
	}
	if os.Getenv(ENV_ROUTER_KAFKA_TOPIC) != "" {
		config.output = os.Getenv(ENV_ROUTER_KAFKA_TOPIC)
	}
	if os.Getenv(ENV_ROUTER_MINIO_ENDPOINT) != "" {
		config.output = os.Getenv(ENV_ROUTER_MINIO_ENDPOINT)
	}
	if os.Getenv(ENV_ROUTER_MINIO_ID) != "" {
		config.output = os.Getenv(ENV_ROUTER_MINIO_ID)
	}
	if os.Getenv(ENV_ROUTER_MINIO_ACCESSKEY) != "" {
		config.output = os.Getenv(ENV_ROUTER_MINIO_ACCESSKEY)
	}
}

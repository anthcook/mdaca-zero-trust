package aws

import (
	"fmt"
	"log"
	"os"
	"strings"

	"mdaca-zero-trust/pkg/parameters"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
)

// SessionConfig represents configuration information for an AWS session.
type SessionConfig struct {
	// The credentials profile to use from the user's ~/.aws/credentials file.
	// ProfileID is mutually exclusive with AccessKey+SecretKey[+SessionToken]
	//
	ProfileID string

	// The IAM account access key and secret key pair to use for credentials,
	// along with the session token if temporary credentials are used.
	// The combination of keys[+token] is mutually exclusive with ProfileID
	//
	AccessKey    string
	SecretKey    string
	SessionToken string

	// The AWS region to use. The user's shared ~/.aws/config file is used if
	// region is not provided.
	//
	Region string
}

func ClientSession() *session.Session {
	return awsClient.awsSession
}

func InitClientSession(config SessionConfig) (bool, error) {
	options := session.Options{}

	if config.Region != "" {
		region := strings.ToLower(config.Region)
		options.Config = aws.Config{
			Region: aws.String(region),
		}

		logIt(fmt.Sprintf("Using provided AWS region: %v", region))
	} else {
		options.SharedConfigState = session.SharedConfigEnable

		region := os.Getenv("AWS_REGION")
		if region == "" {
			region = os.Getenv("AWS_DEFAULT_REGION")
		}
		if region != "" {
			logIt(fmt.Sprintf("Using environment defined region: %v", region))
		} else {
			logIt("Using AWS region from shared configuration.")
		}
	}

	if config.ProfileID != "" {
		options.Profile = config.ProfileID

		logIt(fmt.Sprintf("Using provided AWS credentials profile: %v", config.ProfileID))
	} else {
		if config.AccessKey != "" && config.SecretKey != "" {
			os.Setenv("AWS_ACCESS_KEY_ID", config.AccessKey)
			os.Setenv("AWS_SECRET_ACCESS_KEY", config.SecretKey)

			if config.SessionToken != "" {
				os.Setenv("AWS_SESSION_TOKEN", config.SessionToken)

				logIt("Using provided temporary credentials and token.")
			} else {
				logIt("Using provided credentials.")
			}
		} else {
			accessKey := os.Getenv("AWS_ACCESS_KEY_ID")
			if accessKey == "" {
				accessKey = os.Getenv("AWS_ACCESS_KEY")
			}
			secretKey := os.Getenv("AWS_SECRET_ACCESS_KEY")
			if secretKey == "" {
				secretKey = os.Getenv("AWS_SECRET_KEY")
			}
			if accessKey != "" && secretKey != "" {
				if os.Getenv("AWS_SESSION_TOKEN") != "" {
					logIt("Using environment defined temporary credentials and token.")
				} else {
					logIt("Using environment defined credentials.")
				}
			} else {
				logIt("Using shared credentials file or instance metadata.")
			}
		}
	}

	awsSession, err := session.NewSessionWithOptions(options)
	awsClient.awsSession = awsSession

	success := false
	if err == nil {
		success = true
	}

	return success, err
}

type awsClientSession struct {
	awsSession *session.Session
}

var awsClient = new(awsClientSession)

func logIt(message string) {
	log.Println(fmt.Sprintf("AWS Client - %v", message))
}

func LoadParameters() SessionConfig {
	sessionConfig := SessionConfig{}
	if parameters.AssignedValue(parameters.AWS_REGION) {
		sessionConfig.Region = parameters.Parameter(parameters.AWS_REGION)
	}
	if parameters.AssignedValue(parameters.AWS_PROFILE) {
		sessionConfig.ProfileID = parameters.Parameter(parameters.AWS_PROFILE)
	} else {
		if parameters.AssignedValue(parameters.AWS_ACCESS_KEY) && parameters.AssignedValue(parameters.AWS_SECRET_KEY) {
			sessionConfig.AccessKey = parameters.Parameter(parameters.AWS_ACCESS_KEY)
			sessionConfig.SecretKey = parameters.Parameter(parameters.AWS_SECRET_KEY)

			if parameters.AssignedValue(parameters.AWS_SESSION_TOKEN) {
				sessionConfig.SessionToken = parameters.Parameter(parameters.AWS_SESSION_TOKEN)
			}
		}
	}
	return sessionConfig
}

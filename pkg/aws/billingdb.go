package aws

import (
	"errors"
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

const (
	// device ownership table
	CustomerTable = "CustomerDB"

	// billing metric tracking table
	BillingTable = "BillingDB"

	// billing metric aggegration table
	AggregatedBillingTable = "BillingAggregatedDB"
)

// Customer represents an entry in the device ownership table
type Customer struct {
	DeviceID          string
	CustomerID        string
	CustomerAccountID string
}

// BillingItem represents an entry in the billing metric tracking table
type BillingItem struct {
	CustomerIdTimestampMetric string
	Metric                    string
	Timestamp                 string
	Value                     int
}

// AggegratedBillingItem represents an entry in the billing metric aggegration
// table
type AggegratedBillingItem struct {
	CustomerIdTimestampMetric string
	Value                     int
}

// BillingDBCLient declares methods for accessing the billing DB tables
type BillingDBClient interface {
	// Adds/updates an entry in the device ownership table
	SetDeviceOwner(deviceId string, customerId string, accountId string) (bool, error)

	// Returns device ownership information
	DeviceOwner(deviceId string) (Customer, error)

	// Adds a billing metric to the billing metric tracking table
	AddBillingItem(customerId string, metric string, timestamp string, value int) (bool, error)

	// Returns all billing metrics for a given customer and date range where
	// dates are given in Unix epoch seconds
	BillingItems(customerId string, startDate string, endDate string) ([]BillingItem, error)

	// Adds/updates an aggegrated billing metric to the aggegrated billing
	// metric table
	AddAggregatedBillingItem(customerId string, metric string, timestamp string, value int) (bool, error)
	UpdateAggregatedBillingItem(customerIdTimestampMetric string, value int) (bool, error)

	// Returns the aggregated value for a given billing metric ID
	AggregatedBillingItemValue(customerIdTimestampMetric string) (int, error)
}

// Billing DB client implementation
type billingClient struct {
	hostUrl  string
	dbClient *dynamodb.DynamoDB
}

func (bc billingClient) SetDeviceOwner(deviceId string, customerId string, accountId string) (bool, error) {
	success := true

	customer := Customer{
		DeviceID:          deviceId,
		CustomerID:        customerId,
		CustomerAccountID: accountId,
	}
	av, err := dynamodbattribute.MarshalMap(customer)

	if err != nil {
		logIt(fmt.Sprintf("Error marshalling ownership data for device %v: {CustomerID: %v, CustomerAccountID: %v}",
			deviceId, customerId, accountId))
		success = false
	} else {
		input := &dynamodb.PutItemInput{
			Item:      av,
			TableName: aws.String(CustomerTable),
		}
		_, err = bc.dbClient.PutItem(input)

		if err != nil {
			logIt(fmt.Sprintf("Error updating ownership data on %v: {%v, %v, %v}",
				CustomerTable, deviceId, customerId, accountId))
			success = false
		}
	}

	if success {
		logIt(fmt.Sprintf("Set device owner for device %v: %v.", deviceId, customerId))
	}

	return success, err
}

func (bc billingClient) DeviceOwner(deviceId string) (Customer, error) {
	customer := Customer{}

	result, err := bc.dbClient.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String(CustomerTable),
		Key: map[string]*dynamodb.AttributeValue{
			"DeviceID": {
				S: aws.String(deviceId),
			},
		},
	})
	if err != nil {
		logIt(fmt.Sprintf("Error getting data from %v for device %v.",
			CustomerTable, deviceId))
	} else {
		if result.Item == nil {
			message := fmt.Sprintf("No ownership found for device %v.", deviceId)
			logIt(message)
			err = errors.New(message)
		} else {
			err = dynamodbattribute.UnmarshalMap(result.Item, &customer)
			if err != nil {
				logIt(fmt.Sprintf("Error unmarshalling ownership data from %v for device %v",
					CustomerTable, deviceId))
			}
		}
	}

	return customer, err
}

func (bc billingClient) AddBillingItem(customerId string, metric string, timestamp string, value int) (bool, error) {
	success := true

	item := BillingItem{
		CustomerIdTimestampMetric: fmt.Sprintf("%v%v%v", customerId, timestamp, metric),
		Timestamp:                 timestamp,
		Metric:                    metric,
		Value:                     value,
	}
	av, err := dynamodbattribute.MarshalMap(item)

	if err != nil {
		logIt(fmt.Sprintf("Error marshalling data for %v: {%v, %v, %v, %v}",
			BillingTable, customerId, metric, timestamp, value))
		success = false
	} else {
		input := &dynamodb.PutItemInput{
			Item:      av,
			TableName: aws.String(BillingTable),
		}
		_, err = bc.dbClient.PutItem(input)

		if err != nil {
			logIt(fmt.Sprintf("Error putting data for %v: {%v, %v, %v, %v}",
				BillingTable, customerId, metric, timestamp, value))
			success = false
		}
	}

	if success {
		logIt(fmt.Sprintf("Added billing item for device owner %v: %v.", customerId, item.CustomerIdTimestampMetric))
	}

	return success, err
}

func (bc billingClient) BillingItems(customerId string, startDate string, endDate string) ([]BillingItem, error) {
	var billingItems []BillingItem

	filter := expression.And(
		expression.Name("CustomerIdTimestampMetric").BeginsWith(customerId),
		expression.Name("Timestamp").Between(expression.Value(startDate), expression.Value(endDate)),
	)
	expr, err := expression.NewBuilder().WithFilter(filter).Build()
	if err != nil {
		logIt(fmt.Sprintf("Error building billing scan for customer %v with start date '%v' and end date '%v'",
			customerId, startDate, endDate))
	} else {
		input := &dynamodb.ScanInput{
			ExpressionAttributeNames:  expr.Names(),
			ExpressionAttributeValues: expr.Values(),
			FilterExpression:          expr.Filter(),
			TableName:                 aws.String(BillingTable),
		}

		result, err := bc.dbClient.Scan(input)
		if err != nil {
			logIt(fmt.Sprintf("Error scanning billing data from %v for customer %v with start date '%v' and end date '%v'; err = %v.",
				BillingTable, customerId, startDate, endDate, err))
		} else {
			resultSize := len(result.Items)
			if resultSize < 1 {
				message := fmt.Sprintf("No billing data found for customer %v in range '%v'-'%v'.",
					customerId, startDate, endDate)
				logIt(message)
				err = errors.New(message)
			} else {
				billingItems = make([]BillingItem, resultSize)
				numItems := 0
				for idx, it := range result.Items {
					billingItems[idx] = BillingItem{}
					err = dynamodbattribute.UnmarshalMap(it, &billingItems[idx])
					if err != nil {
						logIt(fmt.Sprintf("Error unmarshalling billing item %v: %v.", it, err))
					} else {
						numItems++
					}
				}

				logIt(fmt.Sprintf("Returning %v billing items for customer %v out of %v scan results.", numItems, customerId, resultSize))
			}
		}
	}

	return billingItems, err
}

func (bc billingClient) AddAggregatedBillingItem(customerId string, metric string, timestamp string, value int) (bool, error) {
	return (bc).UpdateAggregatedBillingItem(fmt.Sprintf("%v%v%v", customerId, timestamp, metric), value)
}

func (bc billingClient) UpdateAggregatedBillingItem(customerIdTimestampMetric string, value int) (bool, error) {
	success := true

	item := AggegratedBillingItem{
		CustomerIdTimestampMetric: customerIdTimestampMetric,
		Value:                     value,
	}
	av, err := dynamodbattribute.MarshalMap(item)

	if err != nil {
		logIt(fmt.Sprintf("Error marshalling data for %v: {%v, %v}",
			AggregatedBillingTable, customerIdTimestampMetric, value))
		success = false
	} else {
		input := &dynamodb.PutItemInput{
			Item:      av,
			TableName: aws.String(AggregatedBillingTable),
		}
		_, err = bc.dbClient.PutItem(input)

		if err != nil {
			logIt(fmt.Sprintf("Error putting data for %v: {%v, %v}",
				AggregatedBillingTable, customerIdTimestampMetric, value))
			success = false
		}
	}

	if success {
		logIt(fmt.Sprintf("Updated aggregated billing item %v: %v.", customerIdTimestampMetric, value))
	}

	return success, err
}

func (bc billingClient) AggregatedBillingItemValue(customerIdTimestampMetric string) (int, error) {
	var value int

	result, err := bc.dbClient.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String(AggregatedBillingTable),
		Key: map[string]*dynamodb.AttributeValue{
			"CustomerIdTimestampMetric": {
				S: aws.String(customerIdTimestampMetric),
			},
		},
	})
	if err != nil {
		logIt(fmt.Sprintf("Error getting data from %v for billing entry %v.",
			AggregatedBillingTable, customerIdTimestampMetric))
	} else {
		if result.Item == nil {
			message := fmt.Sprintf("No aggregated billing data found for entry %v.", customerIdTimestampMetric)
			logIt(message)
			err = errors.New(message)
		} else {
			var bit AggegratedBillingItem
			err = dynamodbattribute.UnmarshalMap(result.Item, &bit)
			if err != nil {
				logIt(fmt.Sprintf("Error unmarshalling aggregated billing data from %v for entry %v",
					AggregatedBillingTable, customerIdTimestampMetric))
			} else {
				value = bit.Value
			}
		}
	}

	return value, err
}

func NewBillingDBClient(hostUrl string) BillingDBClient {
	client := new(billingClient)
	client.hostUrl = hostUrl

	if hostUrl != "" {
		logIt(fmt.Sprintf("Using billing database at URL %v.", hostUrl))

		client.dbClient = dynamodb.New(ClientSession(), &aws.Config{
			Endpoint: aws.String(hostUrl),
		})
	} else {
		logIt("Using billing database in user's cloud account.")

		client.dbClient = dynamodb.New(ClientSession())
	}

	return client
}

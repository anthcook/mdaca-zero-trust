package vault

import (
	"encoding/hex"
	"errors"
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/hashicorp/vault/api"

	"mdaca-zero-trust/pkg/parameters"
)

type VaultType int

var httpClient = &http.Client{
	Timeout: 10 * time.Second,
}

const (
	MockVaultClient VaultType = iota
	RealVaultClient
)

const (
	EncryptionKeyName = "encryptionKey"
	SignatureKeyName  = "signatureKey"
)

type VaultBox struct {
	Path      string
	VaultId   string
	Duration  uint64
	Renewable bool
	Secrets   map[string]string
}

type VaultClient interface {
	EmptyBox(path string) (bool, error)
	InventoryBox(path string) ([]string, error)
	OpenBox(path string) (*VaultBox, error)
	StoreBox(path string, box *VaultBox) (bool, error)
}

func NewBox(path string, id string) *VaultBox {
	return &VaultBox{path, id, 0, false, make(map[string]string)}
}

func Client(clientType VaultType) VaultClient {
	var client VaultClient
	switch clientType {
	case MockVaultClient:
		client = new(mockVault)
	case RealVaultClient:
		client = new(realVault)
	}
	return client
}

// Mock vault client implementation that caches secrets in memory
//
type mockVault struct{}

func (v mockVault) EmptyBox(path string) (bool, error) {
	if path == "" {
		return false, errors.New("Storage path not provided.")
	}

	defer delete(storage, path)
	logIt(fmt.Sprintf("Vault box at path '%v' emptied.", path))

	return true, nil
}

func (v mockVault) InventoryBox(path string) ([]string, error) {
	if path == "" {
		return nil, errors.New("Storage path not provided.")
	}

	var keys []string
	box := storage[path]
	if box == nil {
		return nil, errors.New("Provided box is empty.")
	}

	for key := range box.Secrets {
		keys = append(keys, key)
	}

	logIt(fmt.Sprintf("Returning keys for vault box %v at path '%v': %v.", box.VaultId, path, keys))

	return keys, nil
}

func (v mockVault) OpenBox(path string) (*VaultBox, error) {
	if path == "" {
		return nil, errors.New("Storage path not provided.")
	}

	box := storage[path]
	if box != nil {
		logIt(fmt.Sprintf("Returning vault box %v at path '%v'.", box.VaultId, path))
	}
	return box, nil
}

func (v mockVault) StoreBox(path string, box *VaultBox) (bool, error) {
	if path == "" {
		return false, errors.New("Storage path not provided.")
	}
	if len(box.Secrets) < 1 {
		return false, errors.New("Vault box with no secrets provided.")
	}
	storage[path] = box
	logIt(fmt.Sprintf("Vault box '%v' stored at path '%v'.", box.VaultId, path))
	return true, nil
}

// Real vault client implementation that stores secrets in a Hashicorp Vault
// instance
//
type realVault struct{}

func (v realVault) EmptyBox(path string) (bool, error) {
	if path == "" {
		return false, errors.New("Storage path not provided.")
	}

	inputData := map[string]interface{}{
		"data": map[string]string{},
	}

	instance := StartClient()

	output, err := instance.Logical().Write("secret/data/"+path, inputData)
	fmt.Println(output.Data)
	if err != nil {
		return false, err
	}
	logIt(fmt.Sprintf("Vault box at path '%v' emptied.", path))
	return true, nil
}

func (v realVault) InventoryBox(path string) ([]string, error) {
	if path == "" {
		return nil, errors.New("Storage path not provided.")
	}

	instance := StartClient()

	data, err := instance.Logical().Read("secret/data/" + path)
	if err != nil {
		return nil, err
	}

	var keys []string
	tempData := data.Data["data"].(map[string]interface{})

	for key := range tempData {
		keys = append(keys, key)
	}

	if len(keys) == 0 {
		return nil, errors.New("Provided box is empty.")
	}
	s := strings.Split(path, "/")
	logIt(fmt.Sprintf("Returning keys for vault box %v at path '%v': %v.", s[len(s)-1], path, keys))
	return keys, nil
}

func (v realVault) OpenBox(path string) (*VaultBox, error) {
	if path == "" {
		return nil, errors.New("Storage path not provided.")
	}

	instance := StartClient()
	data, err := instance.Logical().Read("secret/data/" + path)
	if err != nil {
		return nil, err
	}

	if data == nil {
		return nil, errors.New("Provided box does not exist.")
	}

	tempData := data.Data["data"].(map[string]interface{})
	keyMap := make(map[string]string)

	for key, keys := range tempData {
		str := fmt.Sprint(keys)
		keyMap[key] = str
	}
	if len(keyMap) == 0 {
		return nil, errors.New("Provided box is empty.")
	}
	s := strings.Split(path, "/")
	box := &VaultBox{path, s[len(s)-1], uint64(data.LeaseDuration), data.Renewable, keyMap}
	logIt(fmt.Sprintf("Returning vault box %v at path '%v'.", box.VaultId, path))

	return box, nil
}

func (v realVault) StoreBox(path string, box *VaultBox) (bool, error) {
	if path == "" {
		return false, errors.New("Storage path not provided.")
	}
	if len(box.Secrets) < 1 {
		return false, errors.New("Vault box with no secrets provided.")
	}

	inputData := map[string]interface{}{
		"data": box.Secrets,
	}

	instance := StartClient()

	_, err := instance.Logical().Write("secret/data/"+path, inputData)
	if err != nil {
		return false, err
	}
	logIt(fmt.Sprintf("Vault box '%v' stored at path '%v'.", box.VaultId, path))
	return true, nil
}

func StoragePath(id string) string {
	return fmt.Sprintf("kv/%v", id)
}

func FromStorageValue(secret string) ([]byte, error) {
	return hex.DecodeString(secret)
}

func ToStorageValue(secret []byte) string {
	return hex.EncodeToString(secret)
}

var storage map[string]*VaultBox

func init() {
	storage = make(map[string]*VaultBox)
}

func StartClient() *api.Client {
	token := parameters.Parameter(parameters.BROKER_VAULT_TOKEN)
	vaultAddr := parameters.Parameter(parameters.BROKER_VAULT_ADDRESS)
	client, err := api.NewClient(&api.Config{Address: vaultAddr, HttpClient: httpClient})
	if err != nil {
		panic(err)
	}
	client.SetToken(token)
	return client
}

func copyBox(box *VaultBox) *VaultBox {
	newBox := &VaultBox{box.Path, box.VaultId, box.Duration, box.Renewable, make(map[string]string)}
	for key := range box.Secrets {
		newBox.Secrets[key] = ""
	}

	return newBox
}

func logIt(message string) {
	log.Println(fmt.Sprintf("VaultClient - %v", message))
}

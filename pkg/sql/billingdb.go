package sql

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

type Customer struct {
	DeviceID          string
	CustomerID        string
	CustomerAccountID string
}

// Billing DB client implementation
type billingClient struct {
	hostUrl  string
	dbClient *sql.DB
}

type BillingDBClient interface {
	// Returns device ownership information
	SQLDeviceOwner(deviceId string) (Customer, error)
}

func (bc billingClient) SQLDeviceOwner(deviceId string) (Customer, error) {
	customer := Customer{}

	rows, err := bc.dbClient.Query("SELECT DeviceID, CustomerAccountID, CustomerID FROM TestDatabase.CustomerDB where DeviceID =?", deviceId)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	for rows.Next() {
		err := rows.Scan(&customer.DeviceID, &customer.CustomerAccountID, &customer.CustomerID)
		if err != nil {
			log.Fatal(err)
		}
	}
	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}
	return customer, err
}

func NewSQLBillingDBClient(hostUrl string) BillingDBClient {
	client := new(billingClient)
	client.hostUrl = hostUrl
	var err error

	if hostUrl != "" {
		logBillingDB(fmt.Sprintf("Using billing database at URL %v.", hostUrl))

		client.dbClient, err = sql.Open("mysql", fmt.Sprint("root:password@tcp(", hostUrl, ")/TestDatabase"))

		if err != nil {
			panic(err.Error())
		}

	} else {
		logBillingDB("Using billing database in user's cloud account.")

		client.dbClient, err = sql.Open("mysql", "root:password@tcp(127.0.0.1:3306)/TestDatabase")

		if err != nil {
			panic(err.Error())
		}

	}

	return client
}

func logBillingDB(message string) {
	log.Println(fmt.Sprintf("SQL Client - %v", message))
}

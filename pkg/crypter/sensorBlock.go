package crypter

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/sha256"
	"encoding/base64"
	"encoding/binary"
	"encoding/hex"
	"errors"
	"fmt"

	"golang.org/x/crypto/ssh"
)

// Data block field lengths
const (
	SensorDataLength      = 4
	ParamIDLength         = 2
	NodeIDLength          = 9
	TimestampLength       = 4
	InitVectorLength      = 16
	EncryptedDataLength   = 16
	DigestLength          = 32
	SignatureLength       = 64
	DigestableBlockLength = (InitVectorLength + EncryptedDataLength + ParamIDLength + NodeIDLength + TimestampLength)
	MinimumBlockLength    = (ParamIDLength + NodeIDLength + TimestampLength + InitVectorLength + EncryptedDataLength + DigestLength + SignatureLength)
)

// IotDevice represents Vitro IoT device with access to a single sensor data
// block.
type IotDevice interface {
	// Return the data block's parameter ID field.
	ParameterID() *[]byte

	// Return the data block's raw node ID field.
	NodeID() *[]byte

	// Return the data block's node ID field as hex string.
	NodeIDValue() string

	// Return the data block's raw timestamp field.
	Timestamp() *[]byte

	// Return the data block's timestamp field as Unix int (number of seconds
	// since epoch).
	TimestampValue() uint32

	// Return the data block's raw initialization vector for the encryption key
	// used to encrypt the node data value.
	InitVector() *[]byte

	// Return the data block's raw [encrypted] node data field.
	NodeData() *[]byte

	// Return the data block's node data field unencrypted with the given
	// encryption key.
	NodeDataValue(*[]byte) (*[]byte, error)

	// Return the data block's node data field, unencrypted with the given
	// encryption key, as a hex string.
	NodeDataValueHex(*[]byte) (string, error)

	// Return the data block's raw digest field.
	BlockDigest() *[]byte

	// Return the data block's digest field as hex string.
	BlockDigestValue() string

	// Return true if the data block's digest field matches the given data.
	BlockDigestEquals(*[]byte) bool

	// Return the data block's raw signature field.
	BlockSignature() *[]byte

	// Return true if the data block's signature field validates with the given
	// encryption key.
	BlockSignatureValid(*[]byte) (bool, error)
}

// SensorBlock represents a decoded and parsed Vitro IoT sensor data block with
// the following binary format:
//
//   "PID(2)|[NodeID(9)|Timestamp(4)|IV(16)|Data(?)|Hash(32)|Signature(64)]"
//
type SensorBlock struct {
	paramId   []byte
	nodeId    []byte
	timestamp []byte
	iv        []byte
	data      []byte
	digest    []byte
	signature []byte
}

func (sb SensorBlock) ParameterID() *[]byte {
	return &sb.paramId
}

func (sb SensorBlock) NodeID() *[]byte {
	return &sb.nodeId
}

func (sb SensorBlock) NodeIDValue() string {
	return hex.EncodeToString(sb.nodeId)
}

func (sb SensorBlock) Timestamp() *[]byte {
	return &sb.timestamp
}

func (sb SensorBlock) TimestampValue() uint32 {
	return binary.BigEndian.Uint32(sb.timestamp)
}

func (sb SensorBlock) InitVector() *[]byte {
	return &sb.iv
}

func (sb SensorBlock) NodeData() *[]byte {
	return &sb.data
}

func (sb SensorBlock) NodeDataValue(key *[]byte) (*[]byte, error) {
	dataValue := make([]byte, len(*sb.NodeData()))
	copy(dataValue, *sb.NodeData())
	var err error

	dataLen := len(dataValue)
	if dataLen >= aes.BlockSize {
		modulus := dataLen % aes.BlockSize
		if modulus == 0 && key != nil && len(*key) > 0 {
			cblock, err := aes.NewCipher(*key)
			if err == nil {
				cbc := cipher.NewCBCDecrypter(cblock, *sb.InitVector())
				cbc.CryptBlocks(dataValue, dataValue)
				if bytes.Equal(dataValue, *sb.NodeData()) {
					err = errors.New(fmt.Sprintf("Decryption failure detected; no change in data."))
				} else {
					dataValue = dataValue[:SensorDataLength]
				}
			}
		} else {
			err = errors.New(fmt.Sprintf("Invalid encrypted data block modulus %v, expected 0", modulus))
		}
	} else {
		err = errors.New(fmt.Sprintf("Invalid encrypted data length = %v, expected >= %v", dataLen, aes.BlockSize))
	}

	return &dataValue, err
}

func (sb SensorBlock) NodeDataValueHex(key *[]byte) (string, error) {
	decryptedData, err := (sb).NodeDataValue(key)
	if err != nil {
		return "", err
	}
	return hex.EncodeToString(*decryptedData), nil
}

func (sb SensorBlock) BlockDigest() *[]byte {
	return &sb.digest
}

func (sb SensorBlock) BlockDigestValue() string {
	return hex.EncodeToString(sb.digest)
}

func (sb SensorBlock) BlockDigestEquals(data *[]byte) bool {
	return (bytes.Compare(sb.digest, *data) == 0)
}

func (sb SensorBlock) BlockSignature() *[]byte {
	return &sb.signature
}

func (sb SensorBlock) BlockSignatureValid(key *[]byte) (bool, error) {
	var err error

	verified, blockHash := verifyHash(&sb)
	if verified {
		sigPublicKey, _, err := parseECDSA(*key)
		if err == nil {
			verified = ecdsa.VerifyASN1(&sigPublicKey, blockHash, *sb.BlockSignature())
			if !verified {
				err = errors.New("Block signature verification failed.")
			}
		} else {
			errors.New(fmt.Sprintf("Error recovering signer's public key: %v", err))
		}
	} else {
		err = errors.New("Block digest verification failed.")
	}

	return verified, err
}

// End SensorBlock/IotDevice implementation

// Parses an encoded block of node sensor data into a parsable structure.
func ParseEncodedBlock(encodedData string) (*SensorBlock, error) {
	data, err := base64.StdEncoding.DecodeString(encodedData)
	if err != nil {
		logIt("Error decoding data block.")
		return (&SensorBlock{}), err
	}
	return ParseBlock(data)
}

// Parses a raw block of node sensor data into a parsable structure.
func ParseBlock(data []byte) (*SensorBlock, error) {
	blockLen := len(data)
	if blockLen < MinimumBlockLength {
		return (&SensorBlock{}),
			errors.New(fmt.Sprintf("Invalid data block: length = %v, expected >= %v.",
				blockLen, MinimumBlockLength))
	}

	dataLen := blockLen - (MinimumBlockLength - EncryptedDataLength)
	block := new(SensorBlock)
	offset := 0
	// Parameter ID
	block.paramId = make([]byte, ParamIDLength)
	copy(block.paramId, data[offset:offset+ParamIDLength])
	offset += ParamIDLength
	// NodeID
	block.nodeId = make([]byte, NodeIDLength)
	copy(block.nodeId, data[offset:offset+NodeIDLength])
	offset += NodeIDLength
	// Timestamp
	block.timestamp = make([]byte, TimestampLength)
	copy(block.timestamp, data[offset:offset+TimestampLength])
	offset += TimestampLength
	// Initialization Vector
	block.iv = make([]byte, InitVectorLength)
	copy(block.iv, data[offset:offset+InitVectorLength])
	offset += InitVectorLength
	// Data block
	block.data = make([]byte, dataLen)
	copy(block.data, data[offset:offset+dataLen])
	offset += dataLen
	// Digest
	block.digest = make([]byte, DigestLength)
	copy(block.digest, data[offset:offset+DigestLength])
	offset += DigestLength
	// Signature
	block.signature = make([]byte, SignatureLength)
	copy(block.signature, data[offset:offset+SignatureLength])

	return block, nil
}

func verifyHash(block *SensorBlock) (bool, []byte) {
	// compute new hash value
	hashData := append(*block.InitVector(), *block.NodeData()...)
	hashData = append(hashData, *block.ParameterID()...)
	hashData = append(hashData, *block.NodeID()...)
	hashData = append(hashData, *block.Timestamp()...)
	hashValue := sha256.Sum256(hashData)
	digest := hashValue[:]

	return block.BlockDigestEquals(&digest), digest
}

// parseECDSA parses an ECDSA key according to RFC 5656, section 3.1.
func parseECDSA(in []byte) (ecdsa.PublicKey, []byte, error) {
	var pubKey ecdsa.PublicKey
	var rest []byte
	var err error

	var w struct {
		Curve    string
		KeyBytes []byte
		Rest     []byte `ssh:"rest"`
	}

	if err = ssh.Unmarshal(in, &w); err != nil {
		err = errors.New(fmt.Sprintf("Failed to unmarshal public key; err = %v", err))
	} else {
		rest = w.Rest
		pubKey := new(ecdsa.PublicKey)

		switch w.Curve {
		case "nistp256":
			pubKey.Curve = elliptic.P256()
		case "nistp384":
			pubKey.Curve = elliptic.P384()
		case "nistp521":
			pubKey.Curve = elliptic.P521()
		default:
			err = errors.New("Failed to obtain supported curve type.")
		}

		if err == nil {
			pubKey.X, pubKey.Y = elliptic.Unmarshal(pubKey.Curve, w.KeyBytes)
			if pubKey.X == nil || pubKey.Y == nil {
				err = errors.New("Failed to obtain valid curve point.")
			}
		}
	}

	return (ecdsa.PublicKey)(pubKey), rest, err
}

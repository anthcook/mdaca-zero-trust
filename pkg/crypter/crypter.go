package crypter

import (
	"fmt"
	"log"

	"mdaca-zero-trust/pkg/vault"
)

const (
	DecryptionError ErrorType = iota
	ParsingError
	ValidationError
	VaultError
)

type ErrorType int

type CrypterError struct {
	Message string
	Code    ErrorType
}

func DecryptEncodedData(encodedData string, vaultClient vault.VaultClient) (string, *SensorBlock, CrypterError) {
	nodeBlock, gerr := ParseEncodedBlock(encodedData)
	if gerr != nil {
		return "", nodeBlock, CrypterError{
			Message: fmt.Sprintf("Error parsing node data block; err = %v", gerr),
			Code:    ParsingError,
		}
	}

	nodeData, cerr := decryptBlock(nodeBlock, vaultClient)

	return nodeData, nodeBlock, cerr
}

func DecryptData(data []byte, vaultClient vault.VaultClient) (string, *SensorBlock, CrypterError) {
	nodeBlock, gerr := ParseBlock(data)
	if gerr != nil {
		return "", nodeBlock, CrypterError{
			Message: fmt.Sprintf("Error parsing node data block; err = %v", gerr),
			Code:    ParsingError,
		}
	}

	nodeData, cerr := decryptBlock(nodeBlock, vaultClient)

	return nodeData, nodeBlock, cerr
}

func ValidateEncodedData(encodedData string, vaultClient vault.VaultClient) (bool, *SensorBlock, CrypterError) {
	nodeBlock, gerr := ParseEncodedBlock(encodedData)
	if gerr != nil {
		return false, nodeBlock, CrypterError{
			Message: fmt.Sprintf("Error parsing node data block; err = %v", gerr),
			Code:    ParsingError,
		}
	}

	validated, cerr := validateBlock(nodeBlock, vaultClient)

	return validated, nodeBlock, cerr
}

func ValidateData(data []byte, vaultClient vault.VaultClient) (bool, *SensorBlock, CrypterError) {
	nodeBlock, gerr := ParseBlock(data)
	if gerr != nil {
		return false, nodeBlock, CrypterError{
			Message: fmt.Sprintf("Error parsing node data block; err = %v", gerr),
			Code:    ParsingError,
		}
	}

	validated, cerr := validateBlock(nodeBlock, vaultClient)

	return validated, nodeBlock, cerr
}

func decryptBlock(nodeBlock *SensorBlock, vaultClient vault.VaultClient) (string, CrypterError) {
	nodeId := nodeBlock.NodeIDValue()
	encKey, cerr := getVaultKey(nodeId, vault.EncryptionKeyName, vaultClient)
	if cerr != (CrypterError{}) {
		return "", cerr
	}
	encodedValue, gerr := nodeBlock.NodeDataValueHex(&encKey)
	if gerr != nil {
		return encodedValue, CrypterError{
			Message: fmt.Sprintf("Error retrieving encoded node data value for device %v; err = %v", nodeId, gerr),
			Code:    DecryptionError,
		}
	}

	return encodedValue, (CrypterError{})
}

func validateBlock(nodeBlock *SensorBlock, vaultClient vault.VaultClient) (bool, CrypterError) {
	nodeId := nodeBlock.NodeIDValue()
	sigKey, cerr := getVaultKey(nodeId, vault.SignatureKeyName, vaultClient)
	if cerr != (CrypterError{}) {
		return false, cerr
	}

	validated, gerr := nodeBlock.BlockSignatureValid(&sigKey)
	if gerr != nil {
		return validated, CrypterError{
			Message: fmt.Sprintf("Error validating node data signature for device %v; err = %v", nodeId, gerr),
			Code:    ValidationError,
		}
	}

	return validated, (CrypterError{})
}

func getVaultKey(nodeId string, keyName string, vaultClient vault.VaultClient) ([]byte, CrypterError) {
	var key []byte

	var errMesg string
	box, err := vaultClient.OpenBox(vault.StoragePath(nodeId))
	if box == nil {
		errMesg = fmt.Sprintf("No vault keys found for device %v.", nodeId)
	} else if err != nil {
		errMesg = fmt.Sprintf("Error retrieving vault keys for device %v; err = %v", nodeId, err)
	} else {
		keyValue := box.Secrets[keyName]
		//logIt(fmt.Sprintf("Returning key for secret %v: %v", keyName, keyValue))

		if keyValue == "" {
			errMesg = fmt.Sprintf("Key %v not found in box %v.", keyName, box.Path)
		} else {
			key, err = vault.FromStorageValue(keyValue)
			if err != nil {
				errMesg = fmt.Sprintf("Error converting key %v for box %v; err = %v", keyName, box.Path, err)
			}
		}
	}

	if errMesg != "" {
		logIt(errMesg)
		return nil, CrypterError{
			Message: errMesg,
			Code:    VaultError,
		}
	}

	return key, (CrypterError{})
}

func logIt(message string) {
	log.Println(fmt.Sprintf("Crypter - %v", message))
}

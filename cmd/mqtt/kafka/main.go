package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"mdaca-zero-trust/pkg/crypter"
	"mdaca-zero-trust/pkg/parameters"
	"mdaca-zero-trust/pkg/vault"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"
	"time"

	MQTT "github.com/eclipse/paho.mqtt.golang"

	"github.com/robfig/cron/v3"
	"github.com/segmentio/kafka-go"
)

type DataPacket struct {
	Format    string  `json:"format"`
	Payload   []pLoad `json:"payload"`
	Qos       int     `json:"qos"`
	Timestamp int     `json:"timestamp"`
	Topic     string  `json:"topic"`
}

type pLoad struct {
	NodeDataValue string
	NodeId        string
	Timestamp     string
}

var (
	topic          = "message-json"
	broker1Address string
	broker2Address string
	broker3Address string
	secondsToWait  string
	brokerAddress  string
	ClientID       string
	messageTopic   string
	queueType      string
	queueLength    string
)

type IngestionType int8

var msgQueue []MQTT.Message

var f MQTT.MessageHandler = func(client MQTT.Client, msg MQTT.Message) {
	switch queueType {
	case "instant":
		var IncomingMessage DataPacket
		json.Unmarshal(msg.Payload(), &IncomingMessage)
		temp, _ := json.Marshal(IncomingMessage.Payload[0])
		ctx := context.Background()
		produce(ctx, temp)
	case "batched":
		msgQueue = append(msgQueue, msg)
		logKafkaHandler(fmt.Sprintf("Message received, number of messages in queue= %d", len(msgQueue)))
		i, _ := strconv.Atoi(queueLength)
		if len(msgQueue) >= i {
			for _, message := range msgQueue {
				var IncomingMessage DataPacket
				json.Unmarshal(message.Payload(), &IncomingMessage)
				temp, _ := json.Marshal(IncomingMessage.Payload[0])
				ctx := context.Background()
				produce(ctx, temp)
			}
			msgQueue = nil
		}
	case "timed":
		msgQueue = append(msgQueue, msg)
	default:
		var IncomingMessage DataPacket
		json.Unmarshal(msg.Payload(), &IncomingMessage)
		temp, _ := json.Marshal(IncomingMessage.Payload[0])
		ctx := context.Background()
		produce(ctx, temp)
	}
}

func SendEvery() {
	logKafkaHandler(fmt.Sprintf("Messages in queue= %d", len(msgQueue)))
	tempQueue := msgQueue
	if len(tempQueue) > 0 {
		for _, message := range msgQueue {
			var IncomingMessage DataPacket
			json.Unmarshal(message.Payload(), &IncomingMessage)
			temp, _ := json.Marshal(IncomingMessage.Payload[0])
			ctx := context.Background()
			produce(ctx, temp)
			msgQueue = msgQueue[1:]
		}
	} else {
		logKafkaHandler(fmt.Sprintf("No messages in queue, polling again in %s seconds.", secondsToWait))
	}
}

var vaultClient vault.VaultClient

func initVaultClient() {
	var vtype string
	if strings.ToLower(parameters.Parameter(parameters.BROKER_TEST_MODE)) == "true" {
		vaultClient = vault.Client(vault.MockVaultClient)
		vtype = "mock"
	} else {
		vaultClient = vault.Client(vault.RealVaultClient)
		vtype = "real"
	}

	logKafkaHandler(fmt.Sprintf("Vault client initialized (type = %v).", vtype))
}

func produce(ctx context.Context, block []byte) {
	var IncomingPayload pLoad
	json.Unmarshal(block, &IncomingPayload)
	valid, _, _ := crypter.ValidateEncodedData(IncomingPayload.NodeDataValue, vaultClient)
	if !valid {
		logKafkaHandler("Signature validation unsuccessful, will not send message to queue. Node ID: " + IncomingPayload.NodeId + ", timestamp: " + IncomingPayload.Timestamp)
		return
	}
	w := kafka.NewWriter(kafka.WriterConfig{
		Brokers: []string{broker1Address, broker2Address, broker3Address},
		Topic:   topic,
	})

	err := w.WriteMessages(ctx, kafka.Message{
		Key:   []byte(strconv.FormatInt(int64(time.Now().UnixNano()), 10)),
		Value: block,
	})
	if err != nil {
		logKafkaHandler("Could not write message " + err.Error() + (". Is your Kafka cluster correctly set up?"))
		return
	}
	logKafkaHandler(fmt.Sprint("Message sent, key:", strconv.FormatInt(int64(time.Now().UnixNano()), 10)))

}

func SetVariables() {
	if loaded, err := parameters.Parse(); !loaded {
		if err != nil {
			log.Fatal(fmt.Sprintf("Error parsing runtime parameters: %v", err))
		} else {
			logKafkaHandler("(WARN) Failed to parse runtime parameters.")
		}
	}
	broker1Address = parameters.Parameter(parameters.ROUTER_KAFKA_BROKER_1)
	broker2Address = parameters.Parameter(parameters.ROUTER_KAFKA_BROKER_2)
	broker3Address = parameters.Parameter(parameters.ROUTER_KAFKA_BROKER_3)
	secondsToWait = parameters.Parameter(parameters.ROUTER_KAFKA_QUEUE_TIME)
	brokerAddress = parameters.Parameter(parameters.ROUTER_MQTT_ADDRESS)
	ClientID = parameters.Parameter(parameters.ROUTER_KAFKA_ID)
	messageTopic = parameters.Parameter(parameters.ROUTER_KAFKA_TOPIC)
	queueType = parameters.Parameter(parameters.ROUTER_KAFKA_QUEUE_TYPE)
	queueLength = parameters.Parameter(parameters.ROUTER_KAFKA_QUEUE_LENGTH)
}

func main() {
	SetVariables()
	initVaultClient()
	if queueType == "timed" {
		logKafkaHandler(fmt.Sprintf("Queue set to timer, will poll every %s seconds.", secondsToWait))
		cro := cron.New(cron.WithSeconds())
		cro.AddFunc(fmt.Sprintf("@every %ss", secondsToWait), SendEvery)
		cro.Start()
	}
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)

	opts := MQTT.NewClientOptions().AddBroker(brokerAddress)
	opts.SetClientID(ClientID)
	opts.SetDefaultPublishHandler(f)
	topic := messageTopic

	opts.OnConnect = func(c MQTT.Client) {
		if token := c.Subscribe(topic, 0, f); token.Wait() && token.Error() != nil {
			panic(token.Error())
		}
	}
	client := MQTT.NewClient(opts)
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	} else {
		logKafkaHandler("Connected to server.")
	}
	<-c
}

func logKafkaHandler(message string) {
	log.Println(fmt.Sprintf("KafkaHandler - %v", message))
}

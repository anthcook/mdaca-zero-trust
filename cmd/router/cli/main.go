package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"strconv"
	"time"

	"mdaca-zero-trust/pkg/parameters"
	//"mdaca-zero-trust/pkg/sql"

	avro "github.com/hamba/avro/ocf"
	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	"github.com/segmentio/kafka-go"
)

type IoT_Block_Json struct {
	//ClientID  string
	Data      string `json:"NodeDataValue"`
	DeviceID  string `json:"NodeId"`
	Timestamp string `json:"Timestamp"`
}

var (
	topic           = "message-json"
	endpoint        string
	accessKeyID     string
	secretAccessKey string
	broker1Address  string
	broker2Address  string
	broker3Address  string
	bucketName      string
)

// 	  { "name": "ClientID", "type": "string" },

const schema string = `{
	"name": "NodeRecord",
	"type": "record",
	"namespace": "com.spinsys.zerotrust.vitro.model.avro",
	"fields": [
	  { "name": "DeviceID", "type": "string" },
	  { "name": "Data", "type": "string" },
	  { "name": "Timestamp", "type": "string" }
	]
  }`

//var sqlClient sql.BillingDBClient
var minioClient minio.Client

func ConvertToAvro(jsonData []byte) (*bytes.Buffer, IoT_Block_Json) {
	var jsonRecord IoT_Block_Json
	buf := bytes.NewBuffer([]byte{})
	enc, err := avro.NewEncoder(schema, buf)
	if err != nil {
		log.Fatal(err)
	}

	err = json.Unmarshal(jsonData, &jsonRecord)

	//var client sql.Customer
	//client, err = sqlClient.SQLDeviceOwner(jsonRecord.DeviceID)
	//if client.CustomerID == "" {
	//	logRouterHandler("Client not found in database.")
	//	return buf, jsonRecord
	//}
	//jsonRecord.ClientID = client.CustomerID

	err = enc.Encode(jsonRecord)
	if err != nil {
		log.Fatal(err)
	}

	if err := enc.Flush(); err != nil {
		log.Fatal(err)
	}
	return buf, jsonRecord
}

func AddFileToS3(jsonData []byte) error {
	var buf *bytes.Buffer
	var err error
	buf, iotBlock := ConvertToAvro(jsonData)
	if len(buf.Bytes()) == 0 {
		logRouterHandler("Avro conversion failed, will not upload to s3. Node ID: " + iotBlock.DeviceID + ", timestamp: " + iotBlock.Timestamp)
		return err
	}
	_, err = minioClient.PutObject(context.Background(), parameters.Parameter(parameters.ROUTER_S3_BUCKETNAME), fmt.Sprintf("%s/%s.avro", iotBlock.DeviceID, strconv.FormatInt(int64(time.Now().UnixNano()), 10)), bytes.NewReader(buf.Bytes()), bytes.NewReader(buf.Bytes()).Size(), minio.PutObjectOptions{ContentType: "application/avro"})
	if err != nil {
		fmt.Println(err)
		return err
	}
	logRouterHandler(fmt.Sprintf("Successfully uploaded %s/%s.avro to %s", iotBlock.DeviceID, iotBlock.Timestamp, parameters.Parameter(parameters.ROUTER_S3_BUCKETNAME)))
	return err
}

func InitRouterHandler() bool {
	//sqlClient = sql.NewSQLBillingDBClient("127.0.0.1:3306")
	minioClient = InitMinIO()

	return true
}

func main() {
	SetVariables()
	InitRouterHandler()
	ctx := context.Background()
	consume(ctx)
}

func SetVariables() {
	if loaded, err := parameters.Parse(); !loaded {
		if err != nil {
			log.Fatal(fmt.Sprintf("Error parsing runtime parameters: %v", err))
		} else {
			logRouterHandler("(WARN) Failed to parse runtime parameters.")
		}
	}
	endpoint = parameters.Parameter(parameters.ROUTER_MINIO_ENDPOINT)
	accessKeyID = parameters.Parameter(parameters.ROUTER_MINIO_ID)
	secretAccessKey = parameters.Parameter(parameters.ROUTER_MINIO_ACCESSKEY)
	broker1Address = parameters.Parameter(parameters.ROUTER_KAFKA_BROKER_1)
	broker2Address = parameters.Parameter(parameters.ROUTER_KAFKA_BROKER_2)
	broker3Address = parameters.Parameter(parameters.ROUTER_KAFKA_BROKER_3)
	bucketName = parameters.Parameter(parameters.ROUTER_S3_BUCKETNAME)
}

func logRouterHandler(message string) {
	log.Println(fmt.Sprintf("RouterHandler - %v", message))
}

func InitMinIO() minio.Client {
	useSSL := false
	// Initialize minio client object.
	minioClient, err := minio.New(endpoint, &minio.Options{
		Creds:  credentials.NewStaticV4(accessKeyID, secretAccessKey, ""),
		Secure: useSSL,
	})
	if err != nil {
		log.Fatalln(err)
	}

	logRouterHandler(fmt.Sprintf("MinIO Client started")) // minioClient is now setup
	return *minioClient
}

func consume(ctx context.Context) {
	r := kafka.NewReader(kafka.ReaderConfig{
		Brokers: []string{broker1Address, broker2Address, broker3Address},
		Topic:   topic,
		GroupID: "Vitro-IoT",
	})
	for {

		msg, err := r.ReadMessage(ctx)
		if err != nil {
			panic("could not read message " + err.Error())
		}
		// after receiving the message, log its value
		messageBody := msg.Value
		err = AddFileToS3(messageBody)
		if err != nil {
			log.Panic(err)
		} else {
			logRouterHandler("Message Processed.")
		}
	}
}

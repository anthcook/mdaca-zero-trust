package main

import (
	"bytes"
	"context"
	"encoding/json"
	"log"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	avro "github.com/hamba/avro/ocf"
)

type IoT_Block_Json struct {
	ClientID  string `json:"ClientID"`
	Data      string `json:"Data"`
	DeviceID  string `json:"DeviceID"`
	Timestamp string `json:"Timestamp"`
}

const schema string = `{
	"name": "IoT_Block_Example",
	"type": "record",
	"namespace": "Vitro.avro",
	"fields": [
	  {
		"name": "ClientID",
		"type": "string"
	  },
	  {
		"name": "DeviceID",
		"type": "string"
	  },
	  {
		"name": "Data",
		"type": "string"
	  },
	  {
		"name": "Timestamp",
		"type": "string"
	  }
	]
  }`

const (
	AWS_S3_REGION = "us-east-1"
	AWS_S3_BUCKET = "testbucket-123456789123"
)

func ConvertToAvro(jsonData []byte) *bytes.Buffer {
	var jsonRecord IoT_Block_Json
	buf := bytes.NewBuffer([]byte{})
	enc, err := avro.NewEncoder(schema, buf)
	if err != nil {
		log.Fatal(err)
	}

	err = json.Unmarshal(jsonData, &jsonRecord)

	err = enc.Encode(jsonRecord)
	if err != nil {
		log.Fatal(err)
	}

	if err := enc.Flush(); err != nil {
		log.Fatal(err)
	}
	return buf
}

func AddFileToS3(s *session.Session, fileName string, jsonData []byte, wantAvro bool) error {
	var buf *bytes.Buffer
	if wantAvro {
		buf = ConvertToAvro(jsonData)
		fileName = fileName + ".avro"
	} else {
		buf = bytes.NewBuffer(jsonData)
		fileName = fileName + ".json"
	}

	_, err := s3.New(s).PutObject(&s3.PutObjectInput{
		Bucket: aws.String(AWS_S3_BUCKET),
		Key:    aws.String(fileName),
		Body:   bytes.NewReader(buf.Bytes()),
	})
	return err
}

func handler(ctx context.Context, sqsEvent events.SQSEvent) error {
	s, err := session.NewSession(&aws.Config{
		Region: aws.String(AWS_S3_REGION)})
	if err != nil {
		log.Fatal(err)
	}
	for _, message := range sqsEvent.Records {
		rawIn := json.RawMessage(message.Body)
		jsonSqsEvent, _ := rawIn.MarshalJSON()
		err = AddFileToS3(s, "result", jsonSqsEvent, false)
		if err != nil {
			log.Fatal(err)
		}
	}

	return nil
}

func main() {

	lambda.Start(handler)
}

package main

import (
	"bytes"
	"context"
	b64 "encoding/base64"
	"encoding/json"
	"fmt"
	"log"
	"strings"

	"mdaca-zero-trust/pkg/crypter"
	"mdaca-zero-trust/pkg/parameters"
	"mdaca-zero-trust/pkg/vault"

	awsSession "mdaca-zero-trust/pkg/aws"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	avro "github.com/hamba/avro/ocf"
)

type IoT_Block_Json struct {
	ClientID      string
	NodeDataValue string `json:"Data"`
	NodeId        string `json:"DeviceID"`
	Timestamp     string `json:"Timestamp"`
}

const schema string = `{
	"name": "IoT_Block_Example",
	"type": "record",
	"namespace": "Vitro.avro",
	"fields": [
	  {
		"name": "ClientID",
		"type": "string"
	  },
	  {
		"name": "NodeId",
		"type": "string"
	  },
	  {
		"name": "NodeDataValue",
		"type": "string"
	  },
	  {
		"name": "Timestamp",
		"type": "string"
	  }
	]
  }`

const (
	AWS_S3_REGION = "us-east-1"
	AWS_S3_BUCKET = "testbucket-123456789123"
)

var clientSession awsSession.BillingDBClient
var vaultClient vault.VaultClient

func ConvertToAvro(jsonData []byte) (*bytes.Buffer, IoT_Block_Json) {
	var jsonRecord IoT_Block_Json
	buf := bytes.NewBuffer([]byte{})
	enc, err := avro.NewEncoder(schema, buf)
	if err != nil {
		log.Fatal(err)
	}

	err = json.Unmarshal(jsonData, &jsonRecord)
	valid, _, _ := crypter.ValidateData([]byte(jsonRecord.NodeDataValue), vaultClient)
	if !valid {
		logRouterHandler("Signature validation unsuccessful.")
		return buf, jsonRecord
	}
	logRouterHandler("TODO: Add handler for invalid data.")
	var client awsSession.Customer
	client, err = clientSession.DeviceOwner(jsonRecord.NodeId)
	jsonRecord.ClientID = client.CustomerID

	err = enc.Encode(jsonRecord)
	if err != nil {
		log.Fatal(err)
	}

	if err := enc.Flush(); err != nil {
		log.Fatal(err)
	}
	return buf, jsonRecord
}

func AddFileToS3(s *session.Session, jsonData []byte) error {
	var buf *bytes.Buffer
	var err error

	buf, iotBlock := ConvertToAvro(jsonData)
	if len(buf.Bytes()) == 0 {
		logRouterHandler("Avro conversion failed, will not upload to s3. Node ID: " + iotBlock.NodeId + ", timestamp: " + iotBlock.Timestamp)
		return err
	}

	_, err = s3.New(s).PutObject(&s3.PutObjectInput{
		Bucket: aws.String(AWS_S3_BUCKET),
		Key:    aws.String(fmt.Sprintf("%s/%s/%s.avro", iotBlock.ClientID, iotBlock.NodeId, iotBlock.Timestamp)),
		Body:   bytes.NewReader(buf.Bytes()),
	})
	return err
}

func handler(ctx context.Context, sqsEvent events.SQSEvent) error {
	s, err := session.NewSession(&aws.Config{
		Region: aws.String(AWS_S3_REGION)})
	if err != nil {
		log.Fatal(err)
	}
	for _, message := range sqsEvent.Records {
		messageBody := *&message.Body
		decodedBlock, err := b64.StdEncoding.DecodeString(messageBody)
		if err == nil {
			messageBody = string(decodedBlock)
		} else {
			logRouterHandler("Message could not be decoded from Base64, is this message encoded?")
		}
		err = AddFileToS3(s, []byte(messageBody))
		if err != nil {
			log.Fatal(err)
		}
	}

	return nil
}

func main() {
	_, clientSession = InitRouterHandler()
	lambda.Start(handler)
}

func InitRouterHandler() (bool, awsSession.BillingDBClient) {
	if loaded, err := parameters.Parse(); !loaded {
		if err != nil {
			log.Fatal(fmt.Sprintf("Error parsing runtime parameters: %v", err))
		} else {
			logRouterHandler("(WARN) Failed to parse runtime parameters.")
		}
	}

	if !initAWSSession() {
		return false, nil
	}

	client := initBillingDBClient()
	initVaultClient()

	return true, client
}

func initAWSSession() bool {
	sessionConfig := awsSession.LoadParameters()

	success, err := awsSession.InitClientSession(sessionConfig)

	if err != nil {
		logRouterHandler(fmt.Sprintf("Error initializing AWS session: %v", err))
	} else {
		logRouterHandler("AWS client session initialized.")
	}

	return success
}

func initBillingDBClient() awsSession.BillingDBClient {
	var dbUrl string
	if parameters.AssignedValue(parameters.BROKER_DB_ADDRESS) {
		dbUrl = parameters.Parameter(parameters.BROKER_DB_ADDRESS)
	}
	clientSession = awsSession.NewBillingDBClient(dbUrl)
	logRouterHandler("Billing database client initialized.")
	return clientSession
}

func initVaultClient() {
	var vtype string
	if strings.ToLower(parameters.Parameter(parameters.BROKER_TEST_MODE)) == "true" {
		vaultClient = vault.Client(vault.MockVaultClient)
		vtype = "mock"
	} else {
		vaultClient = vault.Client(vault.RealVaultClient)
		vtype = "real"
	}

	logRouterHandler(fmt.Sprintf("Vault client initialized (type = %v).", vtype))
}

func logRouterHandler(message string) {
	log.Println(fmt.Sprintf("RouterHandler - %v", message))
}

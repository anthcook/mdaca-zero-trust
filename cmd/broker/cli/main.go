package main

import (
	"fmt"
	"log"

	"mdaca-zero-trust/pkg/web"
)

func main() {
	logIt("Running MDACA broker for Vitro ZeroTrust IoT")

	if web.InitBrokerHandler() {
		web.StartService(new(web.BrokerServiceHandler), 8080)
	}
}

func logIt(message string) {
	log.Println(fmt.Sprintf("Broker - %v", message))
}

func init() {
	log.SetPrefix("Vitro: ")
}

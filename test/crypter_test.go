package test

import (
	"testing"

	"mdaca-zero-trust/pkg/crypter"
)

var dataBlock []byte

// Test ParseBlock with 'valid' data block
func TestParseBlock(t *testing.T) {
	block, err := crypter.ParseBlock(dataBlock)
	if err != nil {
		t.Fatalf("ParseBlock('%v') = %v, wanted SensorBlock, err = %v", dataBlock, block, err)
	}
}

func init() {
	dataBlock = make([]byte, crypter.MinimumBlockLength+10)
	startIdx := 0
	endIdx := 0
	lengths := [6]int{
		crypter.NodeIDLength,
		crypter.TimestampLength,
		crypter.InitVectorLength,
		10,
		crypter.DigestLength,
		crypter.SignatureLength,
	}
	var byt byte
	byt = 0x01
	for _, len := range lengths {
		startIdx = endIdx
		endIdx += len

		for i := startIdx; i < endIdx; i++ {
			dataBlock[i] = byt
			byt += 0x01
		}
	}
}

package test

import (
	"testing"

	"mdaca-zero-trust/pkg/vault"
)

var storagePath = "kv/abc123"

// Test StoreBox with valid key pairs
func TestStoreBoxKeys(t *testing.T) {
	vaultBox := vault.NewBox("", "123")
	vaultBox.Secrets["encKey"] = "encValue"
	vaultBox.Secrets["sigKey"] = "sigValue"
	vaultBox.VaultId = "FilledBox"
	success, err := vault.StoreBox(storagePath, vaultBox)
	if !success && err != nil {
		t.Fatalf("StoreBox('%v') = %v, wanted true, err = %v", vaultBox.VaultId, success, err)
	}
}

func TestStoreBoxEmpty(t *testing.T) {
	vaultBox := vault.NewBox()
	vaultBox.VaultId = "EmptyBox"
	success, err := vault.StoreBox(storagePath, vaultBox)
	if success || err == nil {
		t.Fatalf("StoreBox('%v') = %v, wanted false, err = %v", vaultBox.VaultId, success, err)
	}
}

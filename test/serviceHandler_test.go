package test

import (
	"fmt"
	"testing"

	"mdaca-zero-trust/pkg/web"
)

type serviceHandler struct{}

func (s serviceHandler) SecureKeys(keys *web.VaultKeys) (bool, web.ServiceError) {
	fmt.Println(fmt.Sprintf("Securing keys for device %v.", keys.NodeID))
	return true, (web.ServiceError{})
}
func (s serviceHandler) UpdateCustomer(cust *web.Customer) (bool, web.ServiceError) {
	return true, (web.ServiceError{})
}
func (s serviceHandler) ValidateData(blocks *web.NodeBlocks) (bool, web.ServiceError) {
	return true, (web.ServiceError{})
}
func (s serviceHandler) DecryptData(blocks *web.NodeBlocks) ([]string, web.ServiceError) {
	return []string{""}, (web.ServiceError{})
}

func TestSecureKeys(t *testing.T) {
	handler := new(serviceHandler)
	success := handleSecureKeys(*handler)
	if !success {
		t.Fatalf("SecureKeys result = %v", success)
	}
}

func handleSecureKeys(handler web.ServiceHandler) bool {
	keys := web.VaultKeys{"Node1", "EncKey", "SigKey"}
	success, _ := handler.SecureKeys(&keys)
	return success
}

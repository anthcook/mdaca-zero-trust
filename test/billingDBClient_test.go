package test

import (
	"fmt"
	"log"
	"strconv"
	"testing"

	"mdaca-zero-trust/pkg/aws"
)

var client aws.BillingDBClient
var dbUrl = "http://localhost:8000"
var awsProfile = "spin-ac"
var deviceId = "bcd234"
var customerId = "2345678901"
var accountId = "8765432109"
var startDate = "1635739200"
var endDate = "1636520400"
var interval = 86400

// Test add/update and retrieve device ownership data
func TestCustomerTable(t *testing.T) {
	setCustomer(t)
	getCustomer(t)
}

// Test add and query billing metric data
func TestBillingTable(t *testing.T) {
	addBillingItems(t)
	getBillingItems(t)
}

// Test add and query billing metric data
func TestAggregatedBillingTable(t *testing.T) {
	addAggregatedBillingItems(t)
	updateAggregatedBillingItems(t)
	getAggregatedBillingItemValues(t)
}

func setCustomer(t *testing.T) {
	success, err := client.SetDeviceOwner(deviceId, customerId, accountId)
	if !success && err != nil {
		t.Fatalf("SetDeviceOwner('%v', '%v', '%v') = %v, wanted true, err = %v",
			deviceId, customerId, accountId, success, err)
	}
}

func getCustomer(t *testing.T) {
	owner, err := client.DeviceOwner(deviceId)
	success := true
	if owner == (aws.Customer{}) && err != nil {
		success = false
	}
	if owner.DeviceID != deviceId || owner.CustomerID != customerId || owner.CustomerAccountID != accountId {
		success = false
	}
	if !success {
		t.Fatalf("DeviceOwner('%v') = %v, wanted {CustomerID: %v, CustomerAccountID: %v}, err = %v",
			deviceId, owner, customerId, accountId, err)
	}
}

func addBillingItems(t *testing.T) {
	cnt := 1
	start, _ := strconv.Atoi(startDate)
	end, _ := strconv.Atoi(endDate)
	for i := start; i < end; i += interval {
		success, err := client.AddBillingItem(customerId, "PutBlock", fmt.Sprintf("%v", i), cnt)
		if err != nil {
			t.Fatalf("AddBillingItem('%v', 'PutBlock', '%v', '%v') = %v, wanted true; err = %v",
				customerId, i, cnt, success, err)
		}
		cnt++
	}
}

func getBillingItems(t *testing.T) []aws.BillingItem {
	items, err := client.BillingItems(customerId, startDate, endDate)
	if err != nil {
		t.Fatalf("BillingItems('%v', '%v', '%v') = %v, wanted [10]BillingItem; err = %v",
			customerId, startDate, endDate, items, err)
	}
	cnt := len(items)
	if cnt != 10 {
		t.Fatalf("BillingItems('%v', '%v', '%v') cnt = %v, wanted 10",
			customerId, startDate, endDate, cnt)
	}
	t.Logf("Billing item 0: %v = %v", items[0].CustomerIdTimestampMetric, items[0].Value)

	return items
}

func addAggregatedBillingItems(t *testing.T) {
	cnt := 1
	start, _ := strconv.Atoi(startDate)
	end, _ := strconv.Atoi(endDate)
	for i := start; i < end; i += interval {
		success, err := client.AddAggregatedBillingItem(customerId, "PutBlock", fmt.Sprintf("%v", i), cnt)
		if err != nil {
			t.Fatalf("AddAggregatedBillingItem('%v', 'PutBlock', '%v', '%v') = %v, wanted true; err = %v",
				customerId, i, cnt, success, err)
		}
		cnt++
	}
}

func updateAggregatedBillingItems(t *testing.T) {
	items := getBillingItems(t)
	for i, bit := range items {
		success, err := client.UpdateAggregatedBillingItem(bit.CustomerIdTimestampMetric, bit.Value+i)
		if err != nil {
			t.Fatalf("UpdateAggregatedBillingItem('%v', '%v') = %v, wanted true; err = %v",
				bit.CustomerIdTimestampMetric, bit.Value+i, success, err)
		}
	}
}

func getAggregatedBillingItemValues(t *testing.T) {
	items := getBillingItems(t)
	for _, bit := range items {
		value, err := client.AggregatedBillingItemValue(bit.CustomerIdTimestampMetric)
		if err != nil {
			t.Fatalf("AggregatedBillingItemValue('%v') = %v, wanted int; err = %v",
				bit.CustomerIdTimestampMetric, value, err)
		}
		if value <= 0 {
			t.Fatalf("AggregatedBillingItemValue('%v') = %v, wanted > 0",
				bit.CustomerIdTimestampMetric, value)
		}
		t.Logf("AggregatedBillingItem value: %v = (billing value) %v, (aggregated value) %v",
			bit.CustomerIdTimestampMetric, bit.Value, value)
	}
}

func init() {
	sessionConfig := aws.SessionConfig{ProfileID: awsProfile}
	_, err := aws.InitClientSession(sessionConfig)
	if err != nil {
		log.Fatal(fmt.Sprintf("Failed to initialize AWS session: %v", err))
	}
	client = aws.NewBillingDBClient(dbUrl)
}
